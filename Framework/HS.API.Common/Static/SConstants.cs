﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HS.API.Common.Static {
    public static class SConstants {

        public static string RPICurrentVersionCode {
            get {
                return "v1";
            }
        }
        internal static string RPIRemoteAddressPrefix {
            get {
                return $"http://localhost:1400/rpi/{RPICurrentVersionCode}";
            }
        }

    }
}