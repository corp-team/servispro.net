﻿using HS.App.Common.Decorators;
using HS.App.Domain.Objects;
using System.Threading.Tasks;
using System.Web.Http;

namespace HS.API.Common.Contracts {

    public interface IApiController {
        string RoutePrefix { get; }

    }
    public interface IApiController<TDomain> : IApiController
        where TDomain : DOBase<TDomain> {
        IHttpActionResult List();
        IHttpActionResult All();
        IHttpActionResult Translations();
        IHttpActionResult Columns();
        IHttpActionResult Fields(EFormType type);
        IHttpActionResult One(int id);
    }
}
