﻿using HS.App.Common.Decorators;
using HS.App.Domain.Aggregate;
using HS.App.Domain.Objects;

using System.Threading.Tasks;
using System.Web.Http;

namespace HS.API.Common.Contracts {

    public interface IRPIController {
        string NodeRpiPrefix { get; }

    }
    public interface IRPIController<TAggregate> : IRPIController
        where TAggregate : AGBase<TAggregate> {
        Task<IHttpActionResult> List();
        Task<IHttpActionResult> All();
        Task<IHttpActionResult> Translations();
        Task<IHttpActionResult> Columns();
        Task<IHttpActionResult> Fields(EFormType type);
        Task<IHttpActionResult> One(int id);
    }
}
