﻿using HS.App.Common.Decorators;
using HS.App.Domain.Aggregate;
using HS.App.Domain.Objects;
using HS.App.Repositories.Relayer;

using HS.API.Common.APIExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace HS.API.Common.BaseClasses {

    public abstract class RPIBase<TDomain, TAggregate> : ApiController
        where TDomain : DOBase<TDomain>
        where TAggregate: AGBase<TAggregate> {

        #region Obligatory Properties
        public abstract string NodeRpiPrefix { get; }

        #endregion

        #region HttpGet Actions
        protected async Task<IHttpActionResult> RPIAll() {

            return Json(await this.MakeGetRequest<IEnumerable<TAggregate>>($"http://localhost:140{1}", $"api/{NodeRpiPrefix}/all"));

        }

        public async Task<IHttpActionResult> RPITranslations() {
            return Json(await this.MakeGetRequest<IDictionary<string, string>>($"http://localhost:140{1}", $"api/{NodeRpiPrefix}/translations"));
        }

        protected async Task<IHttpActionResult> RPIList() {

            return Json(await this.MakeGetRequest<IEnumerable<object>>($"http://localhost:140{1}", $"api/{NodeRpiPrefix}/list"));

        }

        protected async Task<IHttpActionResult> RPIColumns() {

            return Json(await this.MakeGetRequest<IEnumerable<IDictionary<string, object>>>($"http://localhost:140{1}", $"api/{NodeRpiPrefix}/columns"));

        }

        protected async Task<IHttpActionResult> RPIFields(EFormType type) {

            return Json(await this.MakeGetRequest<IEnumerable<IDictionary<string, object>>>($"http://localhost:140{1}", $"api/{NodeRpiPrefix}/fields/{type}"));

        }
        
        protected async Task<IHttpActionResult> RPIOne(int id) {

            return Json(await this.MakeGetRequest<TDomain>($"http://localhost:140{1}", $"api/{NodeRpiPrefix}/one/{id}"));

        }

        #endregion

        #region HttpPost Actions
        protected async Task<IHttpActionResult> RPIInsert([FromBody]TDomain model) {

            return Json(await this.MakePostRequest($"http://localhost:140{1}", $"api/{NodeRpiPrefix}/insert", model));

        }
        protected async Task<IHttpActionResult> RPIEdit(long id) {

            var entity = await SRepository.GenerateDMRepository<TDomain>().SelectSingleByID<long>(id);
            if (entity.Success) {
                return Json(await this.MakePostRequest($"http://localhost:140{1}", $"api/{NodeRpiPrefix}/edit/{id}", entity.SingleResponse));
            } else {
                return Json(entity.Message);
            }

        }
        #endregion

    }

}
