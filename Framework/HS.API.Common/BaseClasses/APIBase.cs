﻿using HS.App.Common.Decorators;
using HS.App.Domain.Aggregate;
using HS.App.Domain.DML;
using HS.App.Domain.Objects;
using HS.App.Domain.Query;
using CSC.App.Extensions.Static;
using HS.App.Repositories.Relayer;
using HS.SPro.Internalization.Strings;
using HS.API.Common.Contracts;
using HS.API.Common.Static;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Web.Http;

namespace HS.API.Common.BaseClasses {

    public abstract class APIBase<TDomain, TAggregate, TController> : ApiController
        where TDomain : DOBase<TDomain>
        where TAggregate : AGBase<TAggregate>
        where TController : ApiController {

        #region Get Methods
        protected IHttpActionResult TakeAll() {

            var response = SRepository.GenerateAGRepository<TAggregate>().SelectAllSync();
            if (response.Success) {
                return Json(response.CollectionResponse.AsEnumerable());
            } else {
                return Json(response.Message);
            }

        }

        protected IHttpActionResult TakeList() {

            var response = SRepository.GenerateDMRepository<TDomain>().SelectAllSync();
            if (response.Success) {
                var list = response.CollectionResponse.Select(q =>
                    new { id = q.ID, text = q.TextValue }).ToList();
                list.Insert(0, new { id = 0L, text = Preformats.FieldPlaceholder.Puts(typeof(TDomain).GetStringResource("Entitlement")) });
                return Json(list.AsEnumerable());
            } else {
                return Json(response.Message);
            }
        }
        protected IHttpActionResult TakeTranslations() {

            var rm = new ResourceManager(typeof(TDomain).GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType);
            var set = rm.GetResourceSet(CultureInfo.CurrentUICulture, true, true);
            var dict = new Dictionary<string, string>();
            foreach (DictionaryEntry it in set) {
                string key = it.Key.ToString();
                string resource = it.Value.ToString();
                dict[key] = resource;
            }
            return Json(dict);

        }

        protected IHttpActionResult TakeColumns(Action<IDictionary<string, object>> modifier = null) {

            var list = PickColumns<TAggregate>((pi) => pi.GetCustomAttribute<GridCellAttribute>() != null).ToList();
            foreach (var item in list) {
                modifier?.Invoke(item);
            }
            return Json(list);

        }

        protected IHttpActionResult TakeFields(EFormType formType, Action<IDictionary<string, object>> modifier = null) {

            var list = PickColumns<TDomain>((pi) => (pi.GetCustomAttribute<FormFieldAttribute>()?.FormType & formType) == formType
                || pi.HasAttribute<HavingCollectionAttribute>()).ToList();
            foreach (var item in list) {
                modifier?.Invoke(item);
            }
            return Json(list);
        }

        protected IHttpActionResult TakeOne(int id) {

            var response = SRepository.GenerateDMRepository<TDomain>().SelectSingleSync(dm => dm.ID == id);
            if (response.Success) {
                return Json(response.SingleResponse);
            } else {
                return Json(response.Message);
            }

        }
        #endregion

        #region Post Methods
        protected IHttpActionResult PostInsert(TDomain model) {

            var response = CR<TDomain>.CRUD(model).Upsert();
            if (response.Success) {
                return Json(response.SingleResponse);
            } else {
                return Json(response.Message);
            }

        }
        protected IHttpActionResult PostEdit(long id, Func<IUpdateClause<TDomain>, IUpdateClause<TDomain>> updater) {

            object result = null;
            Q<TDomain>.SelectAllColumns().Where(u => u.ID == id).ExecuteOne(cursor => {
                if (cursor.ResolvedEntity != null) {
                    updater(cursor.Modify()).PersistUpdate((rowc, entity) => { result = entity; });
                } else {
                    result = new Exception($"id: {id} Not Found");
                }
            }, () => result = new Exception($"noneback"));
            return Json(result);

        }
        protected IHttpActionResult PostDelete(long id) {

            object result = null;
            Action<Exception> fallback = (ex) => result = ex;
            Q<TDomain>.SelectAllColumns().Where(x => x.ID == id).ExecuteOne(xq => {
                xq.Delete(fallback);
            });
            return Json(result);

        }

        #endregion

        #region Special Methods
        protected IEnumerable<IDictionary<string, object>> PickColumns<TModel>(Func<PropertyInfo, bool> filter = null) {

            var resourceType = typeof(TModel).GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType;
            var rm = resourceType != null ? new ResourceManager(resourceType) : null;
            foreach (var col in typeof(TModel).GetProperties()) {
                IApiController fieldDomainApi = this as IApiController;
                IApiController collectionItemDomain = null;
                IApiController navigationDomainApi = null;
                if (col.PropertyType.IsCollection()) {
                    var elementType = col.PropertyType.GetGenericArguments().First();
                    collectionItemDomain = typeof(TController).Assembly.GetTypes().Single(t =>
                        t.Implements(typeof(IApiController<>).MakeGenericType(elementType))).Breath<IApiController>();
                }
                if (col.HasAttribute<NavigationFieldAttribute>()) {
                    var navType = col.GetCustomAttribute<NavigationFieldAttribute>()?.NavigationType;
                    navigationDomainApi = typeof(TController).Assembly.GetTypes().Single(t =>
                        t.Implements(typeof(IApiController<>).MakeGenericType(navType))).Breath<IApiController>();
                }
                var selectables = new List<object>();
                var dataType = GetDataTypeOfEditable(col, ref selectables);
                var dict = new Dictionary<string, object>();
                dict.Add("name", col.Name);
                dict.Add("defaultValue", null);
                dict.Add("formdata", new {
                    placeholder = Preformats.FieldPlaceholder.Puts(typeof(TModel).GetStringResource(col.Name)),
                    title = $"{typeof(TModel).GetStringResource(col.Name)}:",
                    isNavigationProperty = navigationDomainApi != null,
                    isPlainText = dataType == "text" && navigationDomainApi == null,
                    isPlainList = dataType == "selectable",
                    isCollection = dataType == "list",
                    isDateTime = dataType == "datetime",
                    urls = new {
                        list = $"{collectionItemDomain?.RoutePrefix}/list",
                        absolutelist = $"{SConstants.RPIRemoteAddressPrefix}/{collectionItemDomain?.RoutePrefix}/list",
                        navigationList = $"{navigationDomainApi?.RoutePrefix}/list"
                    },
                    routes = new {
                        insert = new {
                            newItem = "insert-form({ domain: '" + navigationDomainApi?.RoutePrefix + "', command: 'new-item' })",
                            selectExisting =
                                "items-table({ domain: '" + navigationDomainApi?.RoutePrefix + "', command: 'from-insert-" + fieldDomainApi?.RoutePrefix + "' })"
                        },
                        edit = new {
                            newItem = "insert-form({ domain: '" + navigationDomainApi?.RoutePrefix + "', command: 'new-item' })",
                            editExisting = "edit-form({ domain: '" + navigationDomainApi?.RoutePrefix + "', command: 'edit-existing' })",
                            selectExisting =
                                "items-table({ domain: '" + navigationDomainApi?.RoutePrefix + "', command: 'from-edit-" + fieldDomainApi?.RoutePrefix + "' })"
                        },
                    },
                    selectables = dataType == "selectable" ? selectables : null
                }
                );
                if (filter == null) {
                    dict.Add("label", col.Name);
                } else if (filter(col)) {
                    dict.Add("label", rm?.GetString(col.Name) ?? col.Name);
                } else {
                    continue;
                }
                yield return dict;
            }

        }

        private static string GetDataTypeOfEditable(PropertyInfo col, ref List<object> selectables) {
            var dataType = "text";
            if (col.PropertyType.OfType<DateTime>()) {
                dataType = "datetime";
            } else if (col.PropertyType.OfType<bool>()) {
                dataType = "boolean";
            } else if (col.PropertyType.IsCollection() || col.HasAttribute<HavingCollectionAttribute>()) {
                dataType = "list";
            } else if (col.HasAttribute<ComputedFieldAttribute>()) {
                dataType = "computed";
                var attr = col.GetCustomAttribute<ComputedFieldAttribute>();
                if (attr.ComputedType == EComputedType.Enum) {
                    dataType = "selectable";
                    foreach (var val in Enum.GetValues(attr.ReferencedType)) {
                        selectables.Add(new { value = Enum.GetName(attr.ReferencedType, val), text = val.GetEnumResource(attr.ReferencedType) });
                    }
                }
            }

            return dataType;
        }

        #endregion

    }
}
