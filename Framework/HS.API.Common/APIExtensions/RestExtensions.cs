﻿using HS.App.Domain.Objects;
using System;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace HS.API.Common.APIExtensions {
    public static class RestExtensions {

        private static HttpClient TakeHttpClientByNode(this ApiController self, string baseAddress) {

            var client = new HttpClient();
            client.BaseAddress = new Uri(baseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;

        }

        public static async Task<TGETResponse> MakeGetRequest<TGETResponse>(this ApiController self, string baseAddress, string route)
            where TGETResponse : class {

            var client = self.TakeHttpClientByNode(baseAddress);
            var response = await client.GetAsync(route);

            if (response.IsSuccessStatusCode) {
                return await response.Content.ReadAsAsync<TGETResponse>(new[] {
                    new JsonMediaTypeFormatter()
                });
            } else {
                return default(TGETResponse);
            }

        }
        public static async Task<TEntity> MakePostRequest<TEntity>(this ApiController self, string baseAddress, string route, TEntity entity)
            where TEntity : class {

            var client = self.TakeHttpClientByNode(baseAddress);
            var response = await client.PostAsync<TEntity>(route, entity, new JsonMediaTypeFormatter());

            if (response.IsSuccessStatusCode) {
                return await response.Content.ReadAsAsync<TEntity>(new[] {
                    new JsonMediaTypeFormatter()
                });
            } else {
                return default(TEntity);
            }

        }

    }
}
