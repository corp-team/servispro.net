﻿using System;

namespace HS.App.Common.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class HavingCollectionAttribute : Attribute {
        // This is a positional argument
        public HavingCollectionAttribute() {
        }
    }
}