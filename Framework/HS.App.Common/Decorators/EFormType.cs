﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HS.App.Common.Decorators {
    [Flags]
    public enum  EFormType {

        Insert = 1,
        Edit = 2,

        //Sunday = 1,
        //Monday = 2,
        //Tuesday = 4,
        //Wednesday = 8,
        //Thursday = 16,
        //Friday = 32,
        //Saturday = 64
    }
}
