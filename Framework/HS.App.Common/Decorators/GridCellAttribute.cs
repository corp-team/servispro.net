﻿using System;

namespace HS.App.Common.Decorators {
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
    public sealed class GridCellAttribute : Attribute {
        public GridCellAttribute() {
        }
        
        public int Order { get; set; }
    }
}
