﻿using HS.App.Common.Decorators;
using System;
using System.Reflection;
using System.Resources;

namespace CSC.App.Extensions.Static {
    public static class DecoratorExtensions {

        public static string GetEnumResource<TEnum>(this TEnum value) {

            return value.GetEnumResource(typeof(TEnum));

        }

        public static string GetEnumResource(this object value, Type enumType) {

            if (enumType.IsEnum) {
                var rm = new ResourceManager(enumType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType);
                return rm?.GetString(Enum.GetName(enumType, value));
            } else {
                throw new Exception("Must be an enum");
            }

        }

        public static string GetStringResource<TResource>(this Type resourceType, string key) {

            return resourceType.GetStringResource(key);

        }

        public static string GetStringResource(this Type resourceType, string key) {

            var rm = new ResourceManager(resourceType.GetCustomAttribute<ResourceLocatorAttribute>()?.ResourceType);
            return rm?.GetString(key);

        }

    }
}
