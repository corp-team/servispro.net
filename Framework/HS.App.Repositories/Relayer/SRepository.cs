﻿using HS.App.Domain.Aggregate;
using HS.App.Domain.Contracts;
using HS.App.Domain.Objects;
using System.Collections.Generic;

namespace HS.App.Repositories.Relayer {
    public static class SRepository {

        public static IGenericRepository<TEntity> GenerateDMRepository<TEntity>()
            where TEntity : DOBase<TEntity> {
            return new DMRepository<TEntity>();
        }

        public static IAGRepository<TAggregate> GenerateAGRepository<TAggregate>()
            where TAggregate : AGBase<TAggregate> {
            return new AGRepository<TAggregate>();
        }

    }
}
