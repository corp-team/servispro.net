﻿using HS.App.Domain.Contracts;
using HS.App.Domain.Objects;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HS.App.Repositories {
    internal class DMRepository<TEntity> : DMSyncRepository<TEntity>, IGenericRepository<TEntity>
        where TEntity : DOBase<TEntity> {

        public DMRepository(Func<Exception, IDMLResponse<TEntity>> fallback) 
            : base(fallback) {

        }

        public DMRepository() : base() {

        }

        public async Task<IDMLResponse<TEntity>> Delete<TKey>(TKey key) {
            return await Task.Run(() => DeleteSync(key));
        }

        public async Task<IDMLResponse<TEntity>> Insert(TEntity entity) {
            return await Task.Run(() => InsertSync(entity));
        }
        public async Task<IDMLResponse<TEntity>> Update<TKey>(TKey key, TEntity entity, params Func<TEntity, object>[] relationSelectors) {
            return await Task.Run(() => UpdateSync(key, entity, relationSelectors));
        }
        public async Task<IDMLResponse<TEntity>> SelectAll() {
            return await Task.Run(() => SelectAllSync());
        }

        public async Task<IDMLResponse<TEntity>> SelectBy(params Expression<Func<TEntity, bool>>[] selectors) {
            return await Task.Run(() => SelectBySync(selectors));
        }

        public async Task<IDMLResponse<TEntity>> SelectSingle(Expression<Func<TEntity, bool>> selector) {
            return await Task.Run(() => SelectSingleSync(selector));
        }

        public async Task<IDMLResponse<TEntity>> SelectLastRecord<TKey>(Expression<Func<TEntity, TKey>> keySelector) {
            return await Task.Run(() => SelectLastRecordSync(keySelector));
        }

        public async Task<IDMLResponse<TEntity>> SelectSingleByID<TKey>(TKey key) {
            return await Task.Run(() => SelectSingleByIDSync(key));
        }

    }
}
