﻿using HS.App.Domain.Contracts;
using HS.App.Domain.DML;
using HS.App.Domain.Enums;
using HS.App.Domain.Objects;
using HS.App.Domain.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HS.App.Repositories {
    internal class DMSyncRepository<TEntity>
        where TEntity : DOBase<TEntity> {

        protected readonly Func<Exception, IDMLResponse<TEntity>> _Fallback;

        public DMSyncRepository(Func<Exception, IDMLResponse<TEntity>> fallback) {
            _Fallback = fallback;
        }
        public DMSyncRepository() {
            _Fallback = (exc) => new DMLResponse<TEntity>() {
                Fault = exc,
                Success = false
            };
        }
        public IDMLResponse<TEntity> DeleteSync<TKey>(TKey key) {

            IDMLResponse<TEntity> response = null;
            Q<TEntity>.SelectAllColumns().Where(x => x.ID == 1).ExecuteOne(xq => {
                xq.Delete((exc) => response = _Fallback(exc));
            }, () => response = new DMLResponse<TEntity>() {
                Fault = new Exception($"Entity ID: {key} not found"),
                Success = false
            });
            return response;

        }

        public IDMLResponse<TEntity> InsertSync(TEntity entity) {

            return CR<TEntity>.CRUD(entity).Upsert();

        }
        public IDMLResponse<TEntity> UpdateSync<TKey>(TKey key, TEntity entity, params Func<TEntity, object>[] relationSelectors) {

            IDMLResponse<TEntity> response = null;
            Q<TEntity>.SelectAllColumns().Where(u => u.ID.Equals(key)).ExecuteOne(cursor => {
                response = new DMLResponse<TEntity>(cursor.ResolvedEntity) {
                    Success = true
                };
            });
            return response;

        }
        public IDMLResponse<TEntity> SelectAllSync() {
            IDMLResponse<TEntity> response = null;
            var list = new List<TEntity>();
            Q<TEntity>.SelectAllColumns().ExecuteMany((query) => {
                list.Add(query.ResolvedEntity);
            });
            response = response ?? new DMLResponse<TEntity>(list);
            return response;
        }

        public IDMLResponse<TEntity> SelectBySync(params Expression<Func<TEntity, bool>>[] selectors) {
            IDMLResponse<TEntity> response = null;
            var list = new List<TEntity>();
            var confinement = Q<TEntity>.SelectAllColumns().Where(selectors.First());
            selectors.Skip(1).ToList().ForEach(sel => confinement = confinement.And(sel));
            confinement.ExecuteMany((query) => {
                list.Add(query.ResolvedEntity);
            });
            response = response ?? new DMLResponse<TEntity>(list);
            return response;
        }

        public IDMLResponse<TEntity> SelectSingleSync(Expression<Func<TEntity, bool>> selector) {
            IDMLResponse<TEntity> response = null;
            Q<TEntity>.SelectAllColumns().Where(selector).ExecuteOne((query) => {
                response = new DMLResponse<TEntity>(query.ResolvedEntity);
            });
            return response;
        }
        public IDMLResponse<TEntity> SelectLastRecordSync<TKey>(Expression<Func<TEntity, TKey>> keySelector) {
            IDMLResponse<TEntity> response = null;
            Q<TEntity>.SelectAllColumns().Order().By(keySelector, EOrderBy.ASC).ExecuteMany((query) => {
                response = new DMLResponse<TEntity>(query.ResolvedEntity);
            });
            return response;
        }

        public IDMLResponse<TEntity> SelectSingleByIDSync<TKey>(TKey key) {
            IDMLResponse<TEntity> response = null;
            Q<TEntity>.SelectAllColumns().Where(e => e.ID.Equals(key)).ExecuteOne((query) => {
                response = new DMLResponse<TEntity>(query.ResolvedEntity);
            });
            return response;
        }

    }
}
