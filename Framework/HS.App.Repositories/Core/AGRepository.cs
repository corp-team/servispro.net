﻿using HS.App.Domain.Aggregate;
using HS.App.Domain.Contracts;
using HS.App.Domain.Objects;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HS.App.Repositories {
    internal class AGRepository<TAggregate> : AGSyncRepository<TAggregate>, IAGRepository<TAggregate>
        where TAggregate : AGBase<TAggregate> {

        public async Task<IDMLResponse<TAggregate>> SelectAll() {
            return await Task.Run(() => SelectAllSync());
        }

        public async Task<IDMLResponse<TAggregate>> SelectBy(params Expression<Func<TAggregate, bool>>[] selectors) {
            return await Task.Run(() => SelectBySync(selectors));
        }

        public async Task<IDMLResponse<TAggregate>> SelectSingle(Expression<Func<TAggregate, bool>> selector) {
            return await Task.Run(() => SelectSingleSync(selector));
        }

    }
}
