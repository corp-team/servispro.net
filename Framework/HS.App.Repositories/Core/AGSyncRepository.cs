﻿using HS.App.Domain.Aggregate;
using HS.App.Domain.Contracts;
using HS.App.Domain.DML;
using HS.App.Domain.Objects;
using HS.App.Domain.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace HS.App.Repositories {
    internal class AGSyncRepository<TAggregate>
        where TAggregate : AGBase<TAggregate> {

        protected Func<Exception, IDMLResponse<TAggregate>> Failback { get; private set; }

        public AGSyncRepository() {
            Failback = (exc) => new DMLResponse<TAggregate>() {
                Fault = exc,
                Success = false
            };
        }
        public IDMLResponse<TAggregate> SelectAllSync() {
            IDMLResponse<TAggregate> response = null;
            var list = new List<TAggregate>();
            AG<TAggregate>.Views().SelectAllColumns().ExecuteList((item) => {
                list.Add(item);
            }, (ex) => response = Failback(ex));
            response = response ?? new DMLResponse<TAggregate>(list);
            return response;
        }

        public IDMLResponse<TAggregate> SelectBySync(params Expression<Func<TAggregate, bool>>[] selectors) {
            IDMLResponse<TAggregate> response = null;
            var list = new List<TAggregate>();
            var confinement = AG<TAggregate>.Views().SelectAllColumns().Where(selectors.First());
            selectors.Skip(1).ToList().ForEach(sel => confinement.AndAlso(sel));
            confinement.ExecuteList((item) => {
                list.Add(item);
            }, (ex) => response = Failback(ex));
            response = response ?? new DMLResponse<TAggregate>(list);
            return response;
        }

        public IDMLResponse<TAggregate> SelectSingleSync(Expression<Func<TAggregate, bool>> selector) {
            IDMLResponse<TAggregate> response = null;
            AG<TAggregate>.Views().SelectAllColumns().Where(selector).ExecuteSingle((item) => {
                response = new DMLResponse<TAggregate>(item);
            }, (ex) => response = Failback(ex));
            return response;
        }
        
    }
}
