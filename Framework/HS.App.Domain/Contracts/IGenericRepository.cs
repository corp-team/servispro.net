﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HS.App.Domain.Contracts {
    public interface IGenericRepository<TEntity> : IAGRepository<TEntity>
            where TEntity : class {

        Task<IDMLResponse<TEntity>> Insert(TEntity entity);
        Task<IDMLResponse<TEntity>> Update<TKey>(TKey key, TEntity entity, params Func<TEntity, object>[] relationSelectors);
        Task<IDMLResponse<TEntity>> Delete<TKey>(TKey key);
        Task<IDMLResponse<TEntity>> SelectSingleByID<TKey>(TKey key);
        Task<IDMLResponse<TEntity>> SelectLastRecord<TKey>(Expression<Func<TEntity, TKey>> keySelector);
        IDMLResponse<TEntity> SelectLastRecordSync<TKey>(Expression<Func<TEntity, TKey>> keySelector);

    }
}
