﻿using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace HS.App.Domain.Contracts {
    public interface IAGRepository<TEntity>
            where TEntity : class {
        Task<IDMLResponse<TEntity>> SelectAll();
        Task<IDMLResponse<TEntity>> SelectBy(params Expression<Func<TEntity, bool>>[] selectors);
        Task<IDMLResponse<TEntity>> SelectSingle(Expression<Func<TEntity, bool>> selector);
        IDMLResponse<TEntity> SelectAllSync();
        IDMLResponse<TEntity> SelectBySync(params Expression<Func<TEntity, bool>>[] selectors);
        IDMLResponse<TEntity> SelectSingleSync(Expression<Func<TEntity, bool>> selector);

    }
}
