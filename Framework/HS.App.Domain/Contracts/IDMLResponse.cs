﻿using System;
using System.Collections.Generic;

namespace HS.App.Domain.Contracts {

    public interface IDMLResponse<TEntity> 
        where TEntity : class {

        string Message { get; }
        bool Success { get; }
        Exception Fault { get; }
        ICollection<TEntity> CollectionResponse { get; }
        TEntity SingleResponse { get; }

    }
}
