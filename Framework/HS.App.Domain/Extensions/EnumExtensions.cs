﻿using HS.App.Domain.Objects;
using CSC.App.Extensions.Static;
using System;

namespace HS.App.Domain.Extensions {

    internal static class EnumExtensions {

        internal static EDataType GetDataType(this Type self) {

            if (self.Equals(typeof(String))) {
                return EDataType.String;
            } else if (self.Equals(typeof(bool))) {
                return EDataType.Bool;
            } else if (self.Equals(typeof(DateTime))) {
                return EDataType.DateTime;
            } else if (self.IsEnum) {
                return EDataType.Enum;
            } else if (self.IsNumericType()) {
                return self.Equals(typeof(int?)) ? EDataType.Int : EDataType.BigInt;
            } else {
                return EDataType.String;
            }

        }

    }

}
