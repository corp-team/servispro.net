﻿using System.ComponentModel;

namespace HS.App.Domain.Enums {
    public enum EOrderBy {
        [Description("DESC")]
        DESC,
        [Description("ASC")]
        ASC
    }
}
