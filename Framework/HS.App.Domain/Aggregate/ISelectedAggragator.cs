﻿
using DM.Domain.Aggregate;
using System;
using System.Linq.Expressions;

namespace HS.App.Domain.Aggregate {

    public interface ISelectedAggragator<T> : IConfinedAggragator<T>
        where T : AGBase<T> {

        IWheredAggragator<T> Where(Expression<Func<T, bool>> selector);

    }

}