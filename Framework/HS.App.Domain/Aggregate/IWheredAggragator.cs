﻿
using DM.Domain.Aggregate;
using System;
using System.Linq.Expressions;

namespace HS.App.Domain.Aggregate {

    public interface IWheredAggragator<T> : IConfinedAggragator<T>
        where T : AGBase<T> {

        IConfinedAggragator<T> AndAlso(Expression<Func<T, bool>> selector);
        IConfinedAggragator<T> OrElse(Expression<Func<T, bool>> selector);

    }

}