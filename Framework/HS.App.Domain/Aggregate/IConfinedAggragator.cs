﻿
using HS.App.Domain.Aggregate;
using System;

namespace DM.Domain.Aggregate {

    public interface IConfinedAggragator<T>
        where T : AGBase<T> {

        void ExecuteList(Action<T> cursor, Action<Exception> failback = null);
        void ExecuteSingle(Action<T> cursor, Action<Exception> failback = null);

    }

}