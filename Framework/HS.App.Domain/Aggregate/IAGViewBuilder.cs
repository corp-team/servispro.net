﻿
using HS.App.Domain.Data;
using HS.App.Domain.Objects;
using System;
using System.Text;

namespace HS.App.Domain.Aggregate {

    public interface IAGViewBuilder<T> where T : AGBase<T> {

        IAGMappedViewBuilder<T> MapsTo(Action<IAGSchemaBuilder> schematizer);
        IDataTools DataTools { get; }
        IAGSchemaBuilder SchemaBuilder { get; }
        StringBuilder QueryString { get; }

    }

}