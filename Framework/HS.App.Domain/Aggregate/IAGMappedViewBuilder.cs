﻿
using HS.App.Domain.Objects;
using System;

namespace HS.App.Domain.Aggregate {

    public interface IAGMappedViewBuilder<T> where T : AGBase<T> {

        IAGSelectedViewBuilder<T> Select<TEntity>(Action<IAGSelectList<TEntity, T>> selectCursor = null)
            where TEntity : DOBase<TEntity>;

    }

}