﻿
using HS.App.Domain.Objects;
using CSC.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace HS.App.Domain.Aggregate {

    internal class AGSelectList<TEntity, T> : IAGSelectList<TEntity, T>
        where TEntity : DOBase<TEntity>
        where T : AGBase<T> {

        public AGSelectList() {
            _SelectedExpressions = new List<Tuple<Expression, Expression>>();
        }

        private List<Tuple<Expression, Expression>> _SelectedExpressions;

        public IAGSelectList<TEntity, T> Map<TProp>(Expression<Func<TEntity, TProp>> columnSelector, Expression<Func<T, TProp>> aliasSelector) {

            _SelectedExpressions.Add(
                Tuple.Create<Expression, Expression>(columnSelector.Body, aliasSelector.Body));
            return this;

        }


        public override string ToString() {

            var str = new StringBuilder();
            foreach (var expkv in _SelectedExpressions) {
                var columnSelector = expkv.Item1;
                var aggSelector = expkv.Item2;
                if (columnSelector as MemberExpression != null) {
                    var prop = columnSelector.ExposeMember();
                    var entity = Activator.CreateInstance(typeof(TEntity)) as IDOBase;
                    str.AppendFormat("{0}.[{1}] AS {2}, ", entity.SchemaBuilder.GetFormatted(),
                        prop.Name, aggSelector.ExposeMember().Name);
                } else {
                    var compositString = new StringBuilder();
                    var entity = Activator.CreateInstance(typeof(TEntity)) as IDOBase;
                    Action<PropertyInfo> caseMember = (pr) => {
                        compositString.AppendFormat(" {0}.[{1}] ",
                            entity.SchemaBuilder.GetFormatted(), pr.Name);
                    };
                    Action<object> caseConstant = (value) => {
                        compositString.AppendFormat("+ '{0}' +", value);
                    };

                    if (columnSelector is BinaryExpression) {
                        var left = (columnSelector as BinaryExpression).Left;
                        if (left is BinaryExpression) {
                            var left1 = (left as BinaryExpression).Left;
                            var left2 = (left as BinaryExpression).Right;
                            left1.ExtractComposit(caseMember, caseConstant);
                            left2.ExtractComposit(caseMember, caseConstant);
                        } else {
                            left.ExtractComposit(caseMember, caseConstant);
                        }
                        (columnSelector as BinaryExpression).ExtractComposit(caseMember, caseConstant);
                    } else if (columnSelector.NodeType == ExpressionType.Call) {
                        throw new Exception("You cannot specify method call in a aggragate mapping. Use a computed mapping instead.");
                    } else {
                        var unit = columnSelector as UnaryExpression;
                        unit.ExtractComposit(caseMember, caseConstant);
                    }

                    str.AppendFormat("{0} AS {1}, ", compositString, aggSelector.ExposeMember().Name);
                }
            }
            return str.ToString().TrimEnd(' ', ',');
        }

    }

}