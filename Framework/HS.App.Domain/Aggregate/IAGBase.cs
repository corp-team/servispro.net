﻿
using HS.App.Domain.Objects;

namespace HS.App.Domain.Aggregate {

    public interface IAGBase : IDomainBaseModel {

        IAGBase Drop();
        void BuildView();
        IAGSchemaBuilder GetSchemaBuilder();

    }

}