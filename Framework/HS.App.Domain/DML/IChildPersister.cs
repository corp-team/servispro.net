﻿
using HS.App.Domain.Contracts;
using HS.App.Domain.Objects;
using System;

namespace HS.App.Domain.DML {

    public interface IChildPersister<TEntity>
        where TEntity: class {

        IDMLResponse<TEntity> Persist();

    }

}