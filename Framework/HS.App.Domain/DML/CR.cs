﻿using HS.App.Domain.Objects;

namespace HS.App.Domain.DML {

    public static class CR<TEntity>
        where TEntity : DOBase<TEntity> {

        public static IChild<TEntity> CRUD(TEntity entity) {
            return new Child<TEntity>(entity);
        }

    }

}
