﻿
using HS.App.Domain.Contracts;
using HS.App.Domain.Objects;
using System;

namespace HS.App.Domain.DML {

    public interface IChild<TEntity>
        where TEntity : DOBase<TEntity> {

        IChild<TEntity> SetAutoIdentity(bool on = true);
        IChild<TOther> Single<TOther>(Func<IExecutedResult<TEntity>, TOther> cursor)
            where TOther : DOBase<TOther>;
        ISibling<TEntity> Sibling();

        void ExecuteMany(Action<IExecutedResultList> cursor, Action<Exception> fallback = null);

        IDMLResponse<TEntity> Upsert(Action<TEntity> callback = null);

    }

}