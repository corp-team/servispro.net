﻿
using System;
using System.Collections.Generic;

namespace HS.App.Domain.DML {

    public interface IEntityDictionary {

        List<KeyValuePair<Type, object>> Dict { get; }

    }

}