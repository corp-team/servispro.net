﻿using HS.App.Domain.Contracts;
using HS.App.Domain.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HS.App.Domain {
    public static class SDomain {
        public static IDMLResponse<TEntity> GenerateDMLResponse<TEntity>(bool success, TEntity singleResponse)
            where TEntity : class {
            return new DMLResponse<TEntity>(singleResponse) { Success = success };
        }

        public static IDMLResponse<TEntity> GenerateDMLResponse<TEntity>(bool success, ICollection<TEntity> multipleResponse)
            where TEntity : class {
            return new DMLResponse<TEntity>(multipleResponse) { Success = success };
        }
        public static IDMLResponse<TEntity> GenerateDMLResponse<TEntity>(bool success, Exception ex)
            where TEntity : class {
            return new DMLResponse<TEntity>() { Fault = ex, Success = success };
        }

    }
}
