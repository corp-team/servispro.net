﻿using HS.App.Domain.Enums;
using HS.App.Domain.Objects;
using System;
using System.Linq.Expressions;

namespace HS.App.Domain.Query {

    public interface IOrderedList<TEntity> : ICursorExecuter<TEntity>
        where TEntity : DOBase<TEntity> {

        IOrderedList<TEntity> By<TProp>(Expression<Func<TEntity, TProp>> selector, EOrderBy orderBy);

    }

}
