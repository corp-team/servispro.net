﻿using HS.App.Domain.Enums;
using HS.App.Domain.Objects;
using CSC.App.Extensions.Static;
using System;
using System.Linq.Expressions;

namespace HS.App.Domain.Query {

    public class OrderedList<TEntity> : IOrderedList<TEntity> 
        where TEntity : DOBase<TEntity> {
        
        private SelectList<TEntity> _SelectList;

        public OrderedList(SelectList<TEntity> selectList) {
            this._SelectList = selectList;
        }
        
        public IOrderedList<TEntity> By<TProp>(Expression<Func<TEntity, TProp>> selector, EOrderBy orderBy) {

            var param = selector.Parameters[0] as ParameterExpression;
            var prop = selector.ResolveMember();
            this._SelectList.QueryBuilder.OrderByField(prop, orderBy);
            return this;

        }

        public void ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action fallback = null) {
            this._SelectList.ExecuteOne(cursor, fallback);
        }

        public void ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null) {
            this._SelectList.ExecuteMany(cursor, noneback);
        }

    }

}