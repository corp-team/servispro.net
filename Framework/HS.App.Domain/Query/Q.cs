﻿using HS.App.Domain.Objects;

namespace HS.App.Domain.Query {

    public static class Q<TEntity> 
        where TEntity : DOBase<TEntity> {

        public static ISelectList<TEntity> SelectAllColumns(bool off = false) {

            return new SelectList<TEntity>(!off);

        }

    }

}
