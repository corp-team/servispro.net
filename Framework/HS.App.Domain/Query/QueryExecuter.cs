﻿
using HS.App.Domain.Enums;
using HS.App.Domain.Objects;
using System;
using System.Collections.Generic;

namespace HS.App.Domain.Query {

    public class QueryExecuter<TEntity> : IQueryExecuter<TEntity>
         where TEntity : DOBase<TEntity> {

        private SelectList<TEntity> _SelectList;

        public QueryExecuter(SelectList<TEntity> selectList) {
            this._SelectList = selectList;
            if (this._SelectList.AllColumns) {
                this._SelectList.QueryBuilder.Fields.AddRange( 
                    Activator.CreateInstance<TEntity>().TakeFields(EResolveBy.All));
            }
        }
        
        public IEnumerable<IExecutedQuery<TEntity>> GenerateExecutedQueryList(Action noneback = null) {
            var commandText = this._SelectList.QueryBuilder.BuildQuery().ToString();
            using (var engine = this._SelectList.QueryBuilder.DataTools.Engine) {
                using (var command = engine.ConnectifiedCommand(commandText)) {
                    using (var reader = command.ExecuteReader()) {
                        var c = 0;
                        if (reader.Read()) {
                            yield return new ExecutedQuery<TEntity>(reader,
                                this._SelectList.QueryBuilder.Fields, c++);
                            while (reader.Read()) {
                                yield return new ExecutedQuery<TEntity>(reader,
                                    this._SelectList.QueryBuilder.Fields, c++);
                            }
                        } else {
                            if (noneback != null) {
                                noneback();
                            }
                        }
                        
                    }
                }
            }
        }

        public void GenerateSingleExecutedQuery(Action<ExecutedQuery<TEntity>> doneback, Action noneback = null) {
            var commandText = this._SelectList.QueryBuilder.BuildQuery().ToString();
            using (var engine = this._SelectList.QueryBuilder.DataTools.Engine) {
                using (var command = engine.ConnectifiedCommand(commandText)) {
                    using (var reader = command.ExecuteReader()) {
                        if (reader.Read()) {
                            doneback(new ExecutedQuery<TEntity>(reader,
                                this._SelectList.QueryBuilder.Fields, 0));
                        } else {
                            if (noneback != null) {
                                noneback();
                            }
                        }
                    }
                }
            }
        }
    }

}