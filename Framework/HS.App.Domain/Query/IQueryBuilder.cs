﻿
using HS.App.Domain.Data;
using HS.App.Domain.Enums;
using HS.App.Domain.Objects;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace HS.App.Domain.Query {

    public interface IQueryBuilder {

        IQueryBuilder AddField(PropertyInfo prop);
        IQueryBuilder RemoveField<TEntity>(PropertyInfo prop)
            where TEntity : DOBase<TEntity>;
        IQueryBuilder AndConstraint(PropertyInfo prop, object value, ExpressionType nodeType);
        IQueryBuilder OrConstraint(PropertyInfo prop, object value, ExpressionType nodeType);
        IQueryBuilder OrderByField(PropertyInfo prop, EOrderBy orderBy);

        StringBuilder BuildQuery();
        List<PropertyInfo> Fields { get; }

        IDataTools DataTools { get; }

    }

}