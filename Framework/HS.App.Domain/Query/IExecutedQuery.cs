﻿using HS.App.Domain.Data;
using HS.App.Domain.Objects;
using System;

namespace HS.App.Domain.Query {

    public interface IExecutedQuery<TEntity>
        where TEntity : DOBase<TEntity> {

        int Index { get; }
        TEntity ResolvedEntity { get; }
        IUpdateClause<TEntity> Modify();
        IDataTools DataTools { get; }
        bool Delete(Action<Exception> fallback = null);

    }

}