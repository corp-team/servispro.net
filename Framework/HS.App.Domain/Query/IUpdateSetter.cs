﻿
using HS.App.Domain.Objects;

namespace HS.App.Domain.Query {

    public interface IUpdateSetter<TEntity>
        where TEntity : DOBase<TEntity> {

        IUpdateClause<TEntity> Set<TProp>(TProp value);

    }

}