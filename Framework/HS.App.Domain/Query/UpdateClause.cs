﻿using HS.App.Domain.Objects;
using CSC.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace HS.App.Domain.Query {
    internal class UpdateClause<TEntity> : IUpdateClause<TEntity>
        where TEntity : DOBase<TEntity> {

        private IExecutedQuery<TEntity> executedQuery;
        private UpdateSetter<TEntity> updateSetter;
        private StringBuilder _UpdateString;
        private IFormatter _SchemaBuilder = Activator.CreateInstance<TEntity>().SchemaBuilder;

        public UpdateClause(IExecutedQuery<TEntity> executedQuery) {
            this.executedQuery = executedQuery;
            _UpdateString = new StringBuilder("UPDATE #table# SET #sets# #where#");
        }

        public void PersistUpdate(Action<int, TEntity> cursor) {

            _UpdateString
                .Replace("#table#", _SchemaBuilder.GetFormatted())
                .Replace("#where#", "WHERE ID=@id")
                .Replace("#sets#", (this.executedQuery as ExecutedQuery<TEntity>)
                    .UpdateParameters.Select(up => "{0}=@{0}".Puts(up.Key.Name))
                    .Aggregate((prev, next) => "{0}, {1}".Puts(prev, next)));
            using (var engine = this.executedQuery.DataTools.GenerateEngine()) {
                using (var command = engine.ConnectifiedCommand(_UpdateString.ToString())) {
                    var values = (this.executedQuery as ExecutedQuery<TEntity>)
                        .UpdateParameters.Select(up =>
                        new KeyValuePair<string, object>(up.Key.Name, Convert.ChangeType(up.Value, up.Key.PropertyType)));
                    foreach (var kv in values) {
                        if (kv.Key != "ID") {
                            engine.AddWithValue("@{0}".Puts(kv.Key), kv.Value);
                        }
                    }
                    engine.AddWithValue("@id", this.executedQuery.ResolvedEntity.ID);
                    var rowc = command.ExecuteNonQuery();
                    cursor(rowc, this.executedQuery.ResolvedEntity);
                }
            }

        }

        public IUpdateSetter<TEntity> Update<TProp>(Expression<Func<TEntity, TProp>> selector) {

            updateSetter = new UpdateSetter<TEntity>(this.executedQuery, selector.ResolveMember());
            return updateSetter;

        }

    }
}