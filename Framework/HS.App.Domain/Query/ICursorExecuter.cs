﻿
using HS.App.Domain.Objects;
using System;

namespace HS.App.Domain.Query {

    public interface ICursorExecuter<TEntity>
         where TEntity : DOBase<TEntity> {

        void ExecuteOne(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);
        void ExecuteMany(Action<IExecutedQuery<TEntity>> cursor, Action noneback = null);

    }

}