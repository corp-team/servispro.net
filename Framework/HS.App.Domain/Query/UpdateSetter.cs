﻿using HS.App.Domain.Objects;
using System.Reflection;

namespace HS.App.Domain.Query {

    internal class UpdateSetter<TEntity> : IUpdateSetter<TEntity> 
        where TEntity : DOBase<TEntity> {

        private PropertyInfo prop;
        private IExecutedQuery<TEntity> executedQuery;

        public UpdateSetter(IExecutedQuery<TEntity> executedQuery, PropertyInfo prop) {
            this.executedQuery = executedQuery;
            this.prop = prop;
        }

        public IUpdateClause<TEntity> Set<TProp>(TProp value) {

            this.prop.SetValue(this.executedQuery.ResolvedEntity, value);
            (this.executedQuery as ExecutedQuery<TEntity>).AddUpdateParameter(this.prop, value);
            return new UpdateClause<TEntity>(this.executedQuery);

        }

    }

}