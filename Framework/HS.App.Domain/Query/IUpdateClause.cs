﻿using HS.App.Domain.Objects;
using System;
using System.Linq.Expressions;

namespace HS.App.Domain.Query {

    public interface IUpdateClause<TEntity>
        where TEntity : DOBase<TEntity> {

        IUpdateSetter<TEntity> Update<TProp>(Expression<Func<TEntity, TProp>> selector);
        void PersistUpdate(Action<int, TEntity> cursor);
    }

}