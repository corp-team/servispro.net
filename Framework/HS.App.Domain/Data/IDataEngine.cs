﻿
using System;
using System.Data;

namespace HS.App.Domain.Data {

    public interface IDataEngine : IDisposable {

        IDbCommand ConnectifiedCommand(string commandText = "");
        IDbCommand IdentityCommand(IDbCommand parentCommand);
        IDataEngine AddWithValue(string name, object value);

    }

}