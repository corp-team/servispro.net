﻿namespace HS.App.Domain.Data {

    public interface IConnectionCredentials {

        string ConnectionString();

    }

}
