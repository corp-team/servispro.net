﻿using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace HS.App.Domain.Data {

    public interface IDataTools : IConnectionCredentials {

        string SqlExpression(ExpressionType nodeType);
        EServerType CurrentServerType { get; }

        IDataEngine Engine { get; }
        IDataEngine GenerateEngine();

        string ParseFields(IEnumerable<PropertyInfo> fields);
        string ParseParams(IEnumerable<PropertyInfo> fields);

    }

}
