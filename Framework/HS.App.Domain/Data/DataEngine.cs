﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace HS.App.Domain.Data {

    internal class DataEngine : IDataEngine {

        private EServerType currentServerType;
        private string connectionString;

        private IDbCommand _Command;
        private IDbCommand Command {
            get {
                if (_Command == null) {
                    switch (this.currentServerType) {
                        case EServerType.MySQL:
                            throw new NotImplementedException("MySql isn't Supported");
                        case EServerType.MSSql:
                            _Command = new SqlCommand();
                            break;
                        default:
                            throw new NotImplementedException("Unsupported option");
                    }
                }
                return _Command;
            }
        }

        public DataEngine(EServerType currentServerType, string connectionString) {
            this.currentServerType = currentServerType;
            this.connectionString = connectionString;
        }

        public IDbCommand ConnectifiedCommand(string commandText = "") {
            switch (this.currentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    var connection = new SqlConnection(this.connectionString);
                    if (!String.IsNullOrEmpty(commandText)) {
                        Command.CommandText = commandText; 
                    }
                    Command.Connection = connection;
                    connection.Open();
                    return Command;
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }
        public IDbCommand IdentityCommand(IDbCommand parentCommand) {
            switch (this.currentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    Command.CommandText = "SELECT @@IDENTITY";
                    Command.Connection = parentCommand.Connection as SqlConnection;
                    return Command;
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

        public IDataEngine AddWithValue(string name, object value) {

            switch (this.currentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    var sqlcommand = Command as SqlCommand;
                    if (value.GetType().Equals(typeof(DateTime))) {
                        var prm = new SqlParameter(name, SqlDbType.DateTime2);
                        prm.Value = value;
                        sqlcommand.Parameters.Add(prm);
                    } else if (value.GetType().Equals(typeof(Guid))) {
                        var prm = new SqlParameter(name, SqlDbType.UniqueIdentifier);
                        prm.Value = value;
                        sqlcommand.Parameters.Add(prm);
                    } else {
                        sqlcommand.Parameters.AddWithValue(name, value);
                    }
                    return this;
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }

        public void Dispose() {
            if (Command.Connection.State == ConnectionState.Open) {
                Command.Connection.Close();
            }
        }
    }

}
