﻿using CSC.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HS.App.Domain.Data {

    internal class DataTools : IDataTools, IConnectionCredentials {

        public string ConnectionString() {

            switch (CurrentServerType) {
                case EServerType.MSSql:
#if DEBUG
                    return @"Server=.\HS;Database=servis-db;Persist Security Info=True;User ID=servis-admin;Password=Servis123.";
#else
                    return "Data Source=.;Initial Catalog=llfdb;Persist Security Info=True;User ID=llfadmin;Password=5VJtb0iA";
#endif
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }


        public EServerType CurrentServerType {
            get {
                return EServerType.MSSql;
            }
        }

        public string SqlExpression(ExpressionType nodeType) {

            switch (CurrentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    switch (nodeType) {
                        case ExpressionType.Equal: return "=";
                        case ExpressionType.NotEqual: return "!=";
                        case ExpressionType.GreaterThanOrEqual: return ">=";
                        case ExpressionType.GreaterThan: return ">";
                        case ExpressionType.LessThanOrEqual: return "<=";
                        case ExpressionType.LessThan: return "<";
                        default:
                            throw new NotImplementedException("Unsupported option: {0}".Puts(nodeType));
                    }
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }

        private DataEngine _DataEngine;

        public IDataEngine Engine {

            get { return (_DataEngine = _DataEngine ?? new DataEngine(CurrentServerType, ConnectionString())); }

        }

        public IDataEngine GenerateEngine() {
            return new DataEngine(CurrentServerType, ConnectionString());
        }

        public string ParseFields(IEnumerable<PropertyInfo> fields) {
            switch (CurrentServerType) {
                case EServerType.MSSql:
                    return fields.Select(pr => pr.Name).Aggregate((prev, next) => prev + ", " + next);
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

        public string ParseParams(IEnumerable<PropertyInfo> fields) {
            switch (CurrentServerType) {
                case EServerType.MSSql:
                    return fields.Select(pr => "@" + pr.Name).Aggregate((prev, next) => prev + ", " + next);
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                default:
                    throw new NotImplementedException("Unsupported option");
            }
        }

    }

}
