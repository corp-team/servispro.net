﻿
using HS.App.Domain.Extensions;
using CSC.App.Extensions.Static;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace HS.App.Domain.Objects {

    internal class DOConstraintBuilder<TEntity> : IDOConstraintBuilder<TEntity>
        where TEntity : DOBase<TEntity> {


        protected List<PropertyInfo> fields = new List<PropertyInfo>();
        protected List<ILineBuilder> propBuilders = new List<ILineBuilder>();
        protected List<ILineBuilder> relationBuilders = new List<ILineBuilder>();
        protected IDOSchemaBuilder schemaBuilder = new DOSchemaBuilder();


        public DORelationBuilder ForeignKey<TKey>(params Expression<Func<TEntity, TKey>>[] selectors) {

            selectors.Select(s => s.ResolveMember()).ToList().ForEach(s => {
                if (!fields.Contains(s)) {
                    propBuilders.Add(new DOPropBuilder(s).IsTypeOf(typeof(TKey)
                        .GetDataType()).IsRequired());
                    fields.Add(s);
                }
            });
            var relationBuilder = new DORelationBuilder(schemaBuilder, selectors.Select(s => s.ResolveMember()));
            relationBuilders.Add(relationBuilder);
            return relationBuilder;

        }

    }

}