﻿using HS.App.Domain.Container;
using HS.App.Domain.Data;
using CSC.App.Extensions.Static;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HS.App.Domain.Objects {
    internal class DOUniqueKeyBuilder : ILineBuilder {

        private IEnumerable<PropertyInfo> uniqueKeyProps;
        private IDOSchemaBuilder hostSchemaBuilder;

        public DOUniqueKeyBuilder(IDOSchemaBuilder hostSchemaBuilder, IEnumerable<PropertyInfo> uniqueKeyProps) {
            this.hostSchemaBuilder = hostSchemaBuilder;
            this.uniqueKeyProps = uniqueKeyProps;
        }

        public string Build() {

            switch (SKernelHost.Kernel.Get<IDataTools>().CurrentServerType) {
                case EServerType.MySQL:
                    throw new NotImplementedException("MySql isn't Supported");
                case EServerType.MSSql:
                    return "CONSTRAINT UQ_{0}_{1} UNIQUE ({2})".Puts(
                        this.hostSchemaBuilder.GetTableName(),
                        this.uniqueKeyProps.Select(p => p.Name).Aggregate((prev, next) => 
                            "{0}_{1}".Puts(prev, next)),
                        this.uniqueKeyProps.Select(p => p.Name).Aggregate((prev, next) => "{0}, {1}".Puts(prev, next))
                    );
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }
    }
}