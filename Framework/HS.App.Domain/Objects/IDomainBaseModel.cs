﻿using Newtonsoft.Json.Linq;

namespace HS.App.Domain.Objects {
    public interface IDomainBaseModel {
        string Slug { get; }
    }
}
