﻿namespace HS.App.Domain.Objects {

    public interface ILineBuilder {

        string Build();

    }

}
