﻿
namespace HS.App.Domain.Objects {

    public enum EQuestionType {

        Test = 0,
        Category,
        Name,
        Image,
        Date,
        Boolean,
        Continuous

    }

    public static class ESoruTipiExtensions {

        public static string Parse(this EQuestionType self) {

            switch (self) {
                case EQuestionType.Test:
                    return "Test";
                case EQuestionType.Category:
                    return "Kategori";
                case EQuestionType.Name:
                    return "Yazılı";
                case EQuestionType.Image:
                    return "Resim";
                case EQuestionType.Date:
                    return "Tarih";
                case EQuestionType.Boolean:
                    return "Doğru/Yanlış";
                case EQuestionType.Continuous:
                    return "Değer Aralığı";
                default:
                    return "Kategori Yok";
            }

        }
    }
}