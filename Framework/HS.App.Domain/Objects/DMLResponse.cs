﻿using HS.App.Domain.Contracts;
using System;
using System.Collections.Generic;

namespace HS.App.Domain.Objects {

    internal class DMLResponse<TEntity> : IDMLResponse<TEntity>
        where TEntity : class {

        private string _Message;

        public Exception Fault { get; internal set; }
        public bool Success { get; internal set; }
        public string Message {
            get {
                return Success ? _Message : Fault.Message;
            }
            set {
                _Message = value;
            }
        }

        public DMLResponse() {
            Success = true;
        }
        public DMLResponse(ICollection<TEntity> collection) : this() {
            this.CollectionResponse = collection;
        }

        public DMLResponse(TEntity item) : this() {
            this.SingleResponse = item;
        }

        public ICollection<TEntity> CollectionResponse { get; private set; }
        public TEntity SingleResponse { get; private set; }

    }
}
