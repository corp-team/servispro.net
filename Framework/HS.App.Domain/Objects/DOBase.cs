﻿using HS.App.Domain.Container;
using HS.App.Domain.Enums;
using CSC.App.Extensions.Static;
using Newtonsoft.Json.Linq;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace HS.App.Domain.Objects {

    public abstract class DOBase<TEntity> : BindableBase, IDOBase
        where TEntity : DOBase<TEntity> {

        public abstract string TextValue { get; }
        protected abstract void Map(IDOTableBuilder<TEntity> builder);


        public DOBase() {

        }

        private IDOTableBuilder<TEntity> _DOBuilder;

        protected IDOTableBuilder<TEntity> TableBuilder {
            get {
                if (_DOBuilder == null) {
                    _DOBuilder = SKernelHost.Kernel.Get<IDOTableBuilder<TEntity>>();
                    Map(TableBuilder);
                }
                return _DOBuilder;
            }
        }

        public IDOSchemaBuilder SchemaBuilder {
            get {
                return TableBuilder.SchemaBuilder;
            }
        }

        public long ID { get; set; }

        public string Slug {
            get {
                return SchemaBuilder.GetTableName().ToLower();
            }
        }

        public IDOBase BuildConstraints() {

            TableBuilder.BuildConstraints();
            return this;

        }

        public void BuildTable() {

            TableBuilder.BuildTable();

        }

        public IEnumerable<DOPropBuilder> TakeAutoGeneratedBuilders() {
            return TableBuilder.AutoGeneratedBuilders();
        }

        public IEnumerable<PropertyInfo> TakeFields(EResolveBy by) {

            switch (by) {
                case EResolveBy.All:
                    return TableBuilder.Fields;
                case EResolveBy.AllAssigned:
                    return TableBuilder.Fields.Where(f => f.IsAssigned(this));
                case EResolveBy.AllRequired:
                    return TableBuilder.RequiredFields();
                case EResolveBy.AllPKs:
                    return TableBuilder.PKFields();
                default:
                    throw new NotImplementedException("Unsupported option");
            }

        }
        
        public IDOBase Drop() {
            TableBuilder.CutOff();
            return this;
        }

        public void Absorb(TEntity other) {

            foreach (var field in TakeFields(EResolveBy.All)) {
                if (field.IsAssigned(other)) {
                    field.SetValue(this, field.GetValue(other)); 
                }
            }

        }

    }

}
