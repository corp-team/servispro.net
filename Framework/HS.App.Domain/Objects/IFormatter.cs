﻿namespace HS.App.Domain.Objects {
    public interface IFormatter : ILineBuilder {

        string GetFormatted();

    }
}
