﻿using HS.App.Common.Decorators;
using HS.App.Repositories.Relayer;

using HS.SPro.Models.Aggregates;
using HS.SPro.Models.DomainObjects.Core;
using HS.API.Common.APIExtensions;
using HS.API.Common.BaseClasses;
using HS.API.Common.Contracts;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace HS.SPro.RPI.Controllers {

    [RoutePrefix("rpi/v1/departments")]
    public class DepartmentsController : RPIBase<Department, DepartmentAG>, IRPIController<DepartmentAG> {

        public override string NodeRpiPrefix {
            get {
                return "departments";
            }
        }

        #region Specific Actions

        [Route("translations")]
        [HttpGet]
        public async Task<IHttpActionResult> Translations() {

            return await base.RPITranslations();

        }

        #endregion

        #region HttpGet Actions
        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> All() {

            return await base.RPIAll();

        }

        [Route("list")]
        [HttpGet]
        public async Task<IHttpActionResult> List() {

            return await base.RPIList();

        }

        [Route("columns")]
        [HttpGet]
        public async Task<IHttpActionResult> Columns() {

            return await base.RPIColumns();

        }

        [Route("fields/{type}")]
        [HttpGet]
        public async Task<IHttpActionResult> Fields(EFormType type) {

            return await base.RPIFields(type);

        }

        [Route("one/{id:int}")]
        [HttpGet]
        public async Task<IHttpActionResult> One(int id) {

            return await base.RPIOne(id);

        }
        
        #endregion

        #region HttpPost Actions
        [Route("insert")]
        [HttpPost]
        public async Task<IHttpActionResult> Insert([FromBody]Department model) {

            return await base.RPIInsert(model);

        }
        [Route("edit/{id}")]
        [HttpPost]
        public async Task<IHttpActionResult> Edit(long id) {

            return await base.RPIEdit(id);

        }
        #endregion

    }
}
