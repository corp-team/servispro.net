﻿using HS.App.Common.Decorators;
using CSC.App.Extensions.Static;
using HS.App.Repositories.Relayer;
using HS.SPro.Models.Aggregates;
using HS.SPro.Models.DomainObjects.Core;
using HS.API.Common.BaseClasses;
using HS.API.Common.Contracts;
using System.Web.Http;

namespace HS.SPro.Node1.Controllers {

    [RoutePrefix("api/currents")]
    public class CurrentsController : APIBase<Current, CurrentAG, CurrentsController>, IApiController<Current> {

        public string RoutePrefix {
            get {
                return "currents";
            }
        }

        #region Specific Actions

        [Route("translations")]
        [HttpGet]
        public IHttpActionResult Translations() {

            return TakeTranslations();

        }

        #endregion

        #region HttpGet Actions
        [Route("all")]
        [HttpGet]
        public IHttpActionResult All() {

            return TakeAll();

        }

        [Route("list")]
        [HttpGet]
        public IHttpActionResult List() {

            return TakeList();

        }
        
        [Route("columns")]
        [HttpGet]
        public IHttpActionResult Columns() {

            return TakeColumns();

        }

        [Route("fields/{type}")]
        [HttpGet]
        public IHttpActionResult Fields(EFormType type) {

            var code = "CSC" + "1".PadLeft(5, '0');
            var lastRecord = SRepository.GenerateDMRepository<Current>().SelectLastRecordSync(c => c.ID);
            if (lastRecord.Success) {
                code = "CSC" + (lastRecord.SingleResponse.CurrentCode.Replace("CSC", "").Replace("0", "").ToInt() + 1).ToString().PadLeft(5, '0');
            }
            return TakeFields(type, field => {
                if (field["name"].ToString() == "CurrentCode") {
                    field["defaultValue"] = code;
                }
            });
        }

        [Route("one/{id:int}")]
        [HttpGet]
        public IHttpActionResult One(int id) {

            return TakeOne(id);

        }
        
        #endregion

        #region HttpPost Actions
        [Route("insert")]
        [HttpPost]
        public IHttpActionResult Insert([FromBody]Current model) {

            return PostInsert(model);

        }
        [Route("edit/{id}")]
        [HttpPost]
        public IHttpActionResult Edit(long id, [FromBody]Current model) {

            return PostEdit(id, (modifier) => {
                return modifier
                    .Update(so => so.FirmName).Set(model.FirmName)
                    .Update(so => so.Phone).Set(model.Phone)
                    .Update(so => so.AddressID).Set(model.AddressID)
                    ;
            });

        }
        [Route("delete")]
        [HttpPost]
        public IHttpActionResult Delete(long id) {

            return PostDelete(id);

        }
        #endregion

    }

}
