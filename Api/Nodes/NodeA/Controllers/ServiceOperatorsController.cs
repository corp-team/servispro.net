﻿using HS.App.Common.Decorators;
using HS.SPro.Models.Aggregates;
using HS.SPro.Models.DomainObjects.Core;
using HS.API.Common.BaseClasses;
using HS.API.Common.Contracts;
using System.Web.Http;

namespace HS.SPro.Node1.Controllers {

    [RoutePrefix("api/serviceoperators")]
    public class ServiceOperatorsController : APIBase<ServiceOperator, ServiceOperatorAG, ServiceOperatorsController>, IApiController<ServiceOperator> {

        public string RoutePrefix {
            get {
                return "serviceoperators";
            }
        }

        #region Specific Actions

        [Route("translations")]
        [HttpGet]
        public IHttpActionResult Translations() {

            return TakeTranslations();

        }

        #endregion

        #region HttpGet Actions
        [Route("all")]
        [HttpGet]
        public IHttpActionResult All() {

            return TakeAll();

        }

        [Route("list")]
        [HttpGet]
        public IHttpActionResult List() {

            return TakeList();

        }
        
        [Route("columns")]
        [HttpGet]
        public IHttpActionResult Columns() {

            return TakeColumns();

        }

        [Route("fields/{type}")]
        [HttpGet]
        public IHttpActionResult Fields(EFormType type) {

            return TakeFields(type);

        }

        [Route("one/{id:int}")]
        [HttpGet]
        public IHttpActionResult One(int id) {

            return TakeOne(id);

        }
        
        #endregion

        #region HttpPost Actions
        [Route("insert")]
        [HttpPost]
        public IHttpActionResult Insert([FromBody]ServiceOperator model) {

            return PostInsert(model);

        }
        [Route("edit/{id}")]
        [HttpPost]
        public IHttpActionResult Edit(long id, [FromBody]ServiceOperator model) {

            return PostEdit(id, (modifier) => {
                return modifier
                    .Update(so => so.FullName).Set(model.FullName)
                    .Update(so => so.MobilePhone).Set(model.MobilePhone);
            });

        }
        [Route("delete")]
        [HttpPost]
        public IHttpActionResult Delete(long id) {

            return PostDelete(id);

        }
        #endregion

    }

}
