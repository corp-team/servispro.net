﻿using HS.App.Common.Decorators;
using CSC.App.Extensions.Static;
using HS.App.Repositories.Relayer;
using HS.SPro.Models.Aggregates;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Models.DomainObjects.Enums;
using HS.API.Common.BaseClasses;
using HS.API.Common.Contracts;
using System;
using System.Web.Http;

namespace HS.SPro.Node1.Controllers {

    [RoutePrefix("api/serviceorders")]
    public class ServiceOrdersController : APIBase<ServiceOrder, ServiceOrderAG, ServiceOrdersController>, IApiController<ServiceOrder> {

        public string RoutePrefix {
            get {
                return "serviceorders";
            }
        }

        #region Specific Actions

        [Route("translations")]
        [HttpGet]
        public IHttpActionResult Translations() {

            return TakeTranslations();

        }

        #endregion

        #region HttpGet Actions
        [Route("all")]
        [HttpGet]
        public IHttpActionResult All() {

            return TakeAll();

        }

        [Route("list")]
        [HttpGet]
        public IHttpActionResult List() {

            return TakeList();

        }
        
        [Route("columns")]
        [HttpGet]
        public IHttpActionResult Columns() {

            return TakeColumns();

        }

        [Route("fields/{type}")]
        [HttpGet]
        public IHttpActionResult Fields(EFormType type) {

            return TakeFields(type);

        }

        [Route("one/{id:int}")]
        [HttpGet]
        public IHttpActionResult One(int id) {

            return TakeOne(id);

        }
        
        #endregion

        #region HttpPost Actions
        [Route("insert")]
        [HttpPost]
        public IHttpActionResult Insert([FromBody]ServiceOrder model) {

            return PostInsert(model);

        }
        [Route("edit/{id}")]
        [HttpPost]
        public IHttpActionResult Edit(long id, [FromBody]ServiceOrder model) {

            return PostEdit(id, (modifier) => {
                return modifier
                    .Update(so => so.Description).Set(model.Description)
                    .Update(so => so.DueDate).Set(model.DueDate)
                    .Update(so => so.FulfilledOn).Set(model.FulfilledOn)
                    .Update(so => so.ServiceFulfillment).Set(model.ServiceFulfillment)
                    .Update(so => so.ServiceType).Set(model.ServiceType);
            });

        }
        [Route("delete")]
        [HttpPost]
        public IHttpActionResult Delete(long id) {

            return PostDelete(id);

        }
        #endregion

    }

}
