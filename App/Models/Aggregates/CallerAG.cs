﻿
using HS.App.Domain.Aggregate;
using CSC.App.Extensions.Static;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Models.DomainObjects.Enums;
using HS.SPro.Models.DomainObjects.Parameters;
using System;
using System.Collections.Generic;
using HS.App.Common.Decorators;

namespace HS.SPro.Models.Aggregates {
    [ResourceLocator(typeof(Resources.CallerAG))]
    public class CallerAG : AGBase<CallerAG> {
        
        public long CallerID { get; set; }

        [GridCell(Order = 1)]
        public string CallerFullName { get; set; }

        [GridCell(Order = 2)]
        public string CallerMobilePhone { get; set; }

        [GridCell(Order = 3)]
        public string CurrentFirmName { get; set; }

        public override long IDProperty {
            get {
                return CallerID;
            }
        }

        protected override void Map(IAGViewBuilder<CallerAG> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("CallerView"))
                .Select<Caller>(li =>
                    li.Map(x => x.ID, x => x.CallerID)
                    .Map(x => x.FullName, x => x.CallerFullName)
                    .Map(x => x.MobilePhone, x => x.CallerMobilePhone)
                )
                .OuterJoin<Current>(cur => cur.Map(y => y.FirmName, x => x.CurrentFirmName))
                    .On<Caller>((x, y) => x.ID == y.CurrentID);
        }
    }
}
