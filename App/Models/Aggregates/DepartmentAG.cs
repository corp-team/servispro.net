﻿
using HS.App.Domain.Aggregate;
using CSC.App.Extensions.Static;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Models.DomainObjects.Enums;
using HS.SPro.Models.DomainObjects.Parameters;
using System;
using System.Collections.Generic;
using HS.App.Common.Decorators;

namespace HS.SPro.Models.Aggregates {
    [ResourceLocator(typeof(Resources.DepartmentAG))]
    public class DepartmentAG : AGBase<DepartmentAG> {
        
        public long DepartmentID { get; set; }

        [GridCell(Order = 1)]
        public string DepartmentDescription{ get; set; }

        [GridCell(Order = 2)]
        public string DepartmentOfficer { get; set; }

        public override long IDProperty {
            get {
                return DepartmentID;
            }
        }

        protected override void Map(IAGViewBuilder<DepartmentAG> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("DepartmentView"))
                .Select<Department>(li =>
                    li.Map(x => x.ID, x => x.DepartmentID)
                    .Map(x => x.Description, x => x.DepartmentDescription)
                    .Map(x => x.ResponsibleOfficer, x => x.DepartmentOfficer)
                );
        }
    }
}
