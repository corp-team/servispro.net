﻿
using HS.App.Domain.Aggregate;
using CSC.App.Extensions.Static;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Models.DomainObjects.Enums;

using System;
using System.Collections.Generic;
using HS.App.Common.Decorators;

namespace HS.SPro.Models.Aggregates {
    [ResourceLocator(typeof(Resources.ServiceOperatorAG))]
    public class ServiceOperatorAG : AGBase<ServiceOperatorAG> {
        
        public long ServiceOperatorID { get; set; }

        [GridCell(Order = 1)]
        public string ServiceOperatorFullName { get; set; }

        [GridCell(Order = 2)]
        public string ServiceOperatorMobilePhone { get; set; }

        public override long IDProperty {
            get {
                return ServiceOperatorID;
            }
        }

        protected override void Map(IAGViewBuilder<ServiceOperatorAG> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("ServiceOperatorView"))
                .Select<ServiceOperator>(li =>
                    li.Map(x => x.ID, x => x.ServiceOperatorID)
                    .Map(x => x.FullName, x => x.ServiceOperatorFullName)
                    .Map(x => x.MobilePhone, x => x.ServiceOperatorMobilePhone)
                );
        }
    }
}
