﻿
using HS.App.Domain.Aggregate;
using CSC.App.Extensions.Static;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Models.DomainObjects.Enums;
using HS.SPro.Models.DomainObjects.Parameters;
using System;
using System.Collections.Generic;
using HS.App.Common.Decorators;

namespace HS.SPro.Models.Aggregates {
    [ResourceLocator(typeof(Resources.CurrentAG))]
    public class CurrentAG : AGBase<CurrentAG> {
        
        public long ServiceOperatorID { get; set; }

        [GridCell(Order = 1)]
        public string CurrentCode { get; set; }

        [GridCell(Order = 2)]
        public string CurrentFirmName { get; set; }

        [GridCell(Order = 3)]
        public string CurrentPhone { get; set; }

        [GridCell(Order = 4)]
        public string AddressDescription { get; set; }

        public override long IDProperty {
            get {
                return ServiceOperatorID;
            }
        }

        protected override void Map(IAGViewBuilder<CurrentAG> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("CurrentView"))
                .Select<Current>(li =>
                    li.Map(x => x.ID, x => x.ServiceOperatorID)
                    .Map(x => x.CurrentCode, x => x.CurrentCode)
                    .Map(x => x.FirmName, x => x.CurrentFirmName)
                    .Map(x => x.Phone, x => x.CurrentPhone)
                )
                .OuterJoin<Address>(cur => cur.Map(y => y.Description, x => x.AddressDescription))
                    .On<Current>((x, y) => x.ID == y.AddressID);
        }
    }
}
