﻿using HS.App.Domain.Aggregate;
using CSC.App.Extensions.Static;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Models.DomainObjects.Enums;

using System;
using System.Collections.Generic;
using HS.App.Common.Decorators;

namespace HS.SPro.Models.Aggregates {
    [ResourceLocator(typeof(Resources.ServiceOrderAG))]
    public class ServiceOrderAG : AGBase<ServiceOrderAG> {

        public long ServiceOrderID { get; set; }

        [GridCell(Order = 1)]
        public string ServiceOrderCurrentCode { get; set; }

        [GridCell(Order = 2)]
        public string ServiceOrderCaller { get; set; }

        [GridCell(Order = 3)]
        public string ServiceOrderCurrentName { get; set; }

        [GridCell(Order = 4)]
        public string ServiceOrderDescription { get; set; }

        [GridCell(Order = 5)]
        public DateTime ServiceOrderDueDate { get; set; }

        [GridCell(Order = 6)]
        public DateTime ServiceOrderFulfilledOn { get; set; }

        public EServiceFulfillment ServiceOrderFulfillment { get; set; }

        [ComputedField(EComputedType.Enum, typeof(EServiceFulfillment))]
        [GridCell(Order = 7)]
        public string ServiceOrderFulfillmentComputed {
            get {
                return ServiceOrderFulfillment.GetEnumResource();
            }
        }

        [GridCell(Order = 8)]
        public string AssignedCallCenterOperatorName { get; set; }

        [GridCell(Order = 9)]
        public string ServiceOrderDepartment { get; set; }
        public string ServiceOperatorMobilePhone { get; set; }

        public override long IDProperty {
            get {
                return ServiceOrderID;
            }
        }

        protected override void Map(IAGViewBuilder<ServiceOrderAG> builder) {

            builder
                .MapsTo(x => x.SchemaName("VW").ViewName("ServiceOrderView"))
                .Select<ServiceOrder>(li =>
                    li.Map(x => x.ID, x => x.ServiceOrderID)
                    .Map(x => x.Description, x => x.ServiceOrderDescription)
                    .Map(x => x.DueDate, x => x.ServiceOrderDueDate)
                    .Map(x => x.ServiceFulfillment, x => x.ServiceOrderFulfillment)
                    .Map(x => x.FulfilledOn, x => x.ServiceOrderFulfilledOn)
                )
                .OuterJoin<Department>(li => li.Map(x => x.Description, x => x.ServiceOrderDepartment))
                    .On<ServiceOrder>((x, y) => x.ID == y.DepartmentID)
                .OuterJoin<Caller>(li => li.Map(x => x.FullName, x => x.ServiceOrderCaller))
                    .On<ServiceOrder>((x, y) => x.ID == y.CallCenterOperatorID)
                .OuterJoin<CallCenterOperator>(li => li.Map(x => x.FullName, x => x.AssignedCallCenterOperatorName))
                    .On<ServiceOrder>((x, y) => x.ID == y.CallCenterOperatorID)
                .OuterJoin<Current>(li => li.Map(x => x.FirmName, x => x.ServiceOrderCurrentName)
                        .Map(x => x.CurrentCode, x => x.ServiceOrderCurrentCode))
                    .On<ServiceOrder>((x, y) => x.ID == y.CurrentID);
        }
    }
}
