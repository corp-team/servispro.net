﻿using HS.App.Domain.Objects;

namespace HS.SPro.Models.Base {
    public abstract class SPBase<TEntity> : DOBase<TEntity>
        where TEntity : DOBase<TEntity> {

        protected sealed override void Map(IDOTableBuilder<TEntity> builder) {
            builder.For(d => d.ID).IsTypeOf(EDataType.BigInt).IsIdentity();
            builder.PrimaryKey(d => d.ID);
            SPMap(builder);
        }

        protected abstract void SPMap(IDOTableBuilder<TEntity> builder);

    }
}
