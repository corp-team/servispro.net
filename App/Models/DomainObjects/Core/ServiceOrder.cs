﻿using HS.App.Domain.Objects;
using CSC.App.Extensions.Static;
using HS.SPro.Models.Base;
using HS.SPro.Models.DomainObjects.Enums;
using System;
using System.Collections.Generic;
using HS.App.Common.Decorators;

namespace HS.SPro.Models.DomainObjects.Core {

    [ResourceLocator(typeof(Resources.ServiceOrder))]
    public class ServiceOrder : SPBase<ServiceOrder> {

        [FormField(EFormType.Insert | EFormType.Edit, Order = 1)]
        public string Description { get; set; }

        public DateTime RequestedOn { get; set; }

        public DateTime DueDate { get; set; }

        public DateTime FulfilledOn { get; set; }
        
        public EServiceFulfillment ServiceFulfillment { get; set; }

        public EServiceType ServiceType { get; set; }

        [ComputedField(EComputedType.Enum, typeof(EServiceFulfillment))]
        [FormField(EFormType.Edit, Order = 3)]
        public string ServiceFulfillmentComputed {
            get {
                return ServiceFulfillment.GetEnumResource();
            }
        }

        [ComputedField(EComputedType.Enum, typeof(EServiceType))]
        [FormField(EFormType.Insert | EFormType.Edit, Order = 4)]
        public string ServiceTypeComputed {
            get {
                return ServiceType.GetEnumResource();
            }
        }

        [HavingCollection, FormField(EFormType.Insert | EFormType.Edit, Order = 5)]
        public IList<ServiceOperator> ServiceOperators { get; }

        public long CallCenterOperatorID { get; set; }

        [NavigationField(typeof(Current))]
        [FormField(EFormType.Insert, Order = 5)]
        public long CurrentID { get; set; }
            
        [NavigationField(typeof(Caller))]
        [FormField(EFormType.Insert, Order = 6)]
        public long CallerID { get; set; }

        [NavigationField(typeof(Department))]
        [FormField(EFormType.Insert, Order = 7)]
        public long DepartmentID { get; set; }

        public override string TextValue {
            get {
                return Description;
            }
        }

        protected override void SPMap(IDOTableBuilder<ServiceOrder> builder) {

            builder.MapsTo(x => { x.SchemaName("SO").TableName("ServiceOrders"); });
            builder.For(d => d.Description).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.RequestedOn).IsTypeOf(EDataType.DateTime);
            builder.For(d => d.DueDate).IsTypeOf(EDataType.DateTime);
            builder.For(d => d.FulfilledOn).IsTypeOf(EDataType.DateTime);
            builder.For(d => d.ServiceFulfillment).IsTypeOf(EDataType.Enum);
            builder.For(d => d.ServiceType).IsTypeOf(EDataType.Enum).IsRequired();

            builder.ForeignKey(x => x.CallCenterOperatorID).References<CallCenterOperator>(y => y.ID);
            builder.ForeignKey(x => x.CurrentID).References<Current>(y => y.ID);
            builder.ForeignKey(x => x.DepartmentID).References<Department>(y => y.ID);
            builder.ForeignKey(x => x.CallerID).References<Caller>(y => y.ID);
        }
        
    }
}
