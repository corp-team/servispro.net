﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;
using HS.App.Common.Decorators;

namespace HS.SPro.Models.DomainObjects.Core {

    [ResourceLocator(typeof(Resources.Caller))]
    public class Caller : SPBase<Caller> {

        [FormField(EFormType.Insert | EFormType.Edit, Order = 1)]
        public string FullName { get; set; }

        [FormField(EFormType.Insert | EFormType.Edit, Order = 2)]
        public string MobilePhone { get; set; }

        [NavigationField(typeof(Current))]
        public long CurrentID { get; set; }
        public override string TextValue {
            get {
                return FullName;
            }
        }

        protected override void SPMap(IDOTableBuilder<Caller> builder) {

            builder.MapsTo(x => { x.SchemaName("SO").TableName("Callers"); });
            builder.For(d => d.FullName).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.MobilePhone).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);

            builder.ForeignKey(x => x.CurrentID).References<Current>(y => y.ID);

        }
    }
}
