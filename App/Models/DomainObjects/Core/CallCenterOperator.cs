﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;

namespace HS.SPro.Models.DomainObjects.Core {
    public class CallCenterOperator : SPBase<CallCenterOperator> {

        public string FullName { get; set; }

        public string Line { get; set; }

        public string MobilePhone { get; set; }

        public string Email { get; set; }

        public override string TextValue {
            get {
                return FullName;
            }
        }

        protected override void SPMap(IDOTableBuilder<CallCenterOperator> builder) {

            builder.MapsTo(x => { x.SchemaName("SO").TableName("CallCenterOperators"); });
            builder.For(d => d.FullName).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.Line).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(12);
            builder.For(d => d.MobilePhone).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(24);
            builder.For(d => d.Email).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(255);

        }
        
    }
}
