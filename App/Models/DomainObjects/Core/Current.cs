﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;
using HS.SPro.Models.DomainObjects.Parameters;
using HS.App.Common.Decorators;

namespace HS.SPro.Models.DomainObjects.Core {

    [ResourceLocator(typeof(Resources.Current))]
    public class Current : SPBase<Current> {

        [FormField(EFormType.Insert | EFormType.Edit, Order = 1)]
        public string CurrentCode { get; set; }

        [FormField(EFormType.Insert | EFormType.Edit, Order = 2)]
        public string FirmName { get; set; }

        [FormField(EFormType.Insert | EFormType.Edit, Order = 3)]
        public string Phone { get; set; }

        public long AddressID { get; set; }
        
        public override string TextValue {
            get {
                return FirmName;
            }
        }

        protected override void SPMap(IDOTableBuilder<Current> builder) {

            builder.MapsTo(x => { x.SchemaName("SO").TableName("Currents"); });
            builder.For(d => d.CurrentCode).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(32);
            builder.For(d => d.FirmName).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.Phone).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(24);

            builder.ForeignKey(x => x.AddressID).References<Address>(y => y.ID);

        }
        
    }
}
