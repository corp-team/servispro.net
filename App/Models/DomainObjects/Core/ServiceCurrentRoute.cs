﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;

namespace HS.SPro.Models.DomainObjects.Core {
    public class ServiceCurrentRoute : SPBase<ServiceCurrentRoute> {

        public string Description { get; set; }

        public long ServiceOrderID { get; set; }
        public long CurrentID { get; set; }
        public override string TextValue {
            get {
                return Description;
            }
        }

        protected override void SPMap(IDOTableBuilder<ServiceCurrentRoute> builder) {

            builder.MapsTo(x => { x.SchemaName("SO").TableName("ServiceCurrentRoutes"); });
            builder.For(d => d.Description).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.ForeignKey(x => x.ServiceOrderID).References<ServiceOrder>(y => y.ID);
            builder.ForeignKey(x => x.CurrentID).References<Current>(y => y.ID);

        }
        
    }
}
