﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;
using HS.App.Common.Decorators;

namespace HS.SPro.Models.DomainObjects.Core {

    [ResourceLocator(typeof(Resources.ServiceOperator))]
    public class ServiceOperator : SPBase<ServiceOperator> {

        [FormField(EFormType.Insert | EFormType.Edit, Order = 1)]
        public string FullName { get; set; }

        [FormField(EFormType.Insert | EFormType.Edit, Order = 2)]
        public string MobilePhone { get; set; }

        public override string TextValue {
            get {
                return FullName;
            }
        }

        protected override void SPMap(IDOTableBuilder<ServiceOperator> builder) {

            builder.MapsTo(x => { x.SchemaName("SO").TableName("ServiceOperators"); });
            builder.For(d => d.FullName).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.MobilePhone).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);

        }
        
    }
}
