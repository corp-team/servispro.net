﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;

namespace HS.SPro.Models.DomainObjects.Core {
    public class ServiceOrderOperator : SPBase<ServiceOrderOperator> {

        public long ServiceOrderID { get; set; }
        public long ServiceOperatorID { get; set; }
        public override string TextValue {
            get {
                return "";
            }
        }

        protected override void SPMap(IDOTableBuilder<ServiceOrderOperator> builder) {

            builder.MapsTo(x => { x.SchemaName("SO").TableName("ServiceOrderOperators"); });
            builder.ForeignKey(x => x.ServiceOrderID).References<ServiceOrder>(y => y.ID);
            builder.ForeignKey(x => x.ServiceOperatorID).References<ServiceOperator>(y => y.ID);

        }
        
    }
}
