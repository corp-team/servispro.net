﻿using HS.App.Common.Decorators;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;
using HS.SPro.Models.DomainObjects.Enums;
using System;
using System.Collections.Generic;

namespace HS.SPro.Models.DomainObjects.Core {
    [ResourceLocator(typeof(Resources.Department))]
    public class Department : SPBase<Department> {

        [FormField(EFormType.Insert | EFormType.Edit, Order = 1)]
        public string Description { get; set; }

        [FormField(EFormType.Insert | EFormType.Edit, Order = 2)]
        public string ResponsibleOfficer { get; set; }

        public override string TextValue {
            get {
                return Description;
            }
        }
        

        protected override void SPMap(IDOTableBuilder<Department> builder) {

            builder.MapsTo(x => { x.SchemaName("SO").TableName("Departments"); });
            builder.For(d => d.Description).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.ResponsibleOfficer).IsTypeOf(EDataType.String).HasMaxLength(64);

        }
    }
}
