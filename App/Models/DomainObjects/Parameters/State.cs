﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;

namespace HS.SPro.Models.DomainObjects.Parameters {
    public class State : SPBase<State> {

        public string Description { get; set; }

        public int PlateCode { get; set; }

        public long CountryID { get; set; }

        public override string TextValue {
            get {
                return Description;
            }
        }

        protected override void SPMap(IDOTableBuilder<State> builder) {

            builder.MapsTo(x => { x.SchemaName("PM").TableName("States"); });
            builder.For(d => d.Description).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            builder.For(d => d.PlateCode).IsTypeOf(EDataType.Int).IsRequired();

            builder.ForeignKey(x => x.CountryID).References<Country>(y => y.ID);

        }
        
    }
}
