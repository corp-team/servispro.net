﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;

namespace HS.SPro.Models.DomainObjects.Parameters {
    public class District : SPBase<District> {

        public string Description { get; set; }

        public long StateID { get; set; }

        public override string TextValue {
            get {
                return Description;
            }
        }
        
        protected override void SPMap(IDOTableBuilder<District> builder) {

            builder.MapsTo(x => { x.SchemaName("PM").TableName("Districts"); });
            builder.For(d => d.Description).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);

            builder.ForeignKey(x => x.StateID).References<State>(y => y.ID);

        }
    }
}
