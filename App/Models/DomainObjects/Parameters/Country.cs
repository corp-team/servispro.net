﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;

namespace HS.SPro.Models.DomainObjects.Parameters {
    public class Country : SPBase<Country> {

        public string Description { get; set; }

        public override string TextValue {
            get {
                return Description;
            }
        }
        
        protected override void SPMap(IDOTableBuilder<Country> builder) {

            builder.MapsTo(x => { x.SchemaName("PM").TableName("Countries"); });
            builder.For(d => d.Description).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);
            
        }
    }
}
