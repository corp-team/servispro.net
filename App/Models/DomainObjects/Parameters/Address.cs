﻿using System;
using System.Collections.Generic;
using HS.App.Domain.Objects;
using HS.SPro.Models.Base;

namespace HS.SPro.Models.DomainObjects.Parameters {
    public class Address : SPBase<Address> {

        public string Description { get; set; }

        public long StateID { get; set; }

        public long DistrictID { get; set; }

        public long CountryID { get; set; }

        public override string TextValue {
            get {
                return Description;
            }
        }

        protected override void SPMap(IDOTableBuilder<Address> builder) {

            builder.MapsTo(x => { x.SchemaName("PM").TableName("Addresses"); });
            builder.For(d => d.Description).IsTypeOf(EDataType.String).IsRequired().HasMaxLength(64);

            builder.ForeignKey(x => x.StateID).References<State>(y => y.ID);
            builder.ForeignKey(x => x.DistrictID).References<District>(y => y.ID);
            builder.ForeignKey(x => x.CountryID).References<Country>(y => y.ID);

        }
        
    }
}
