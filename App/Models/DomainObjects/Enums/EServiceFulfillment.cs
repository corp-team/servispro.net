﻿using HS.App.Common.Decorators;
using HS.SPro.Internalization.Decorators.Enums;

namespace HS.SPro.Models.DomainObjects.Enums {
    [ResourceLocator(typeof(ServiceFulfillment))]
    public enum EServiceFulfillment {

        Pending,
        Working,
        Fulfilled,
        Invoiced

    }
}