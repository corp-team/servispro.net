﻿using HS.App.Common.Decorators;
using HS.SPro.Internalization.Decorators.Enums;

namespace HS.SPro.Models.DomainObjects.Enums {
    [ResourceLocator(typeof(ServiceType))]
    public enum EServiceType {

        HandOn,
        Inventory

    }
}