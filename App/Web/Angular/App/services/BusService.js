﻿App.service('BusService', function () {
    this.emitGetRequest = function ($scope, key, data, result) {
        result = result || "OK";
        $scope.$emit('GET:' + key + ':' + result, data);
    }
    this.emitPostRequest = function ($scope, key, data, result) {
        result = result || "OK";
        $scope.$emit('POST:' + key + ':' + result, data);
    }
    this.emitPutRequest = function ($scope, key, data, result) {
        result = result || "OK";
        $scope.$emit('PUT:' + key + ':' + result, data);
    }
    this.onGetReceived = function ($scope, key, callback, failback) {
        $scope.$on('GET:' + key + ':OK', function (e, data) { callback(data); });
        if (failback) {
            $scope.$on('GET:' + key + ':FAIL', failback);
        }
    }
    this.onPostReceived = function ($scope, key, callback, failback) {
        $scope.$on('POST:' + key + ':OK', function (e, data) { callback(data); });
        if (failback) {
            $scope.$on('POST:' + key + ':FAIL', failback);
        }
    }
    this.onPutReceived = function ($scope, key, callback, failback) {
        $scope.$on('PUT:' + key + ':OK', function (e, data) { callback(data); });
        if (failback) {
            $scope.$on('PUT:' + key + ':FAIL', failback);
        }
    }
});