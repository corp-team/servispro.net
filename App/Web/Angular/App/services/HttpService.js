﻿App.service('HttpService', function ($http, $log, $q) {
    this.getRequest = function (route, version) {
        return $q(function (resolve, reject) {
            $http.get('http://localhost:1400/rpi/'+ (version || "v1") + '/' + route)
                .success(function (data) {
                    resolve(data);
                })
                .error(function (msg, code) {
                    reject(msg);
                    $log.error(msg, code);
                });
        });
    }
    this.postRequest = function (route, model, version) {
        return $q(function (resolve, reject) {
            $http.post('http://localhost:1400/rpi/' + (version || "v1") + '/' + route, model)
                .success(function (data) {
                    resolve(data);
                })
                .error(function (msg, code) {
                    reject(msg);
                    $log.error(msg, code);
                });
        });
    }
});