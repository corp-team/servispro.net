﻿App.controller('EditItemFrameController', ['$scope', '$state', '$stateParams', 'HttpService', 'BusService', function ($scope, $state, $stateParams, hs, bus) {

    $scope.domain = $stateParams.domain;
    $scope.state = $state.current
    $scope.params = $stateParams;
    $scope.command = $stateParams.command;

    function init() {
        $('#edit-container').loading(function (lastback) {
            hs.getRequest($scope.domain + "/translations").then(function (lexicon) {
                $scope.lexicon = lexicon;
                hs.getRequest($scope.domain + "/one/" + $stateParams.id).then(function (item) {
                    bus.emitGetRequest($scope, $scope.domain + ':one', { response: item });
                    lastback();
                });
            });
        }
    }

    function prepare() {
        setTimeout(function () {
            $.fn.editable.defaults.inputclass = 'form-control';
            $.fn.editable.defaults.mode = 'inline';
            $('#edit-from .formdata').each(function (i, el) {
                var defaultValue = $(el).attr("data-default-value");
                if (defaultValue) {
                    $(el).attr('disabled', true).click(function (e) { e.preventDefault(); });
                } else {
                    $(el).editable({ select2: { width: $('#edit-container').width() * 0.35 }, disabled: $scope.command == 'display-item' });
                }
            });
            $('#edit-from').screen($('#edit-container'), $scope.command == 'display-item');
        }, 100);
    }

    $scope.submitEdit = function () {
        console.log("submitedit");
        hs.postRequest($scope.domain + "/edit/1", $scope.model).then(function (data) {
            bus.emitPutRequest($scope, $scope.domain, { rows: rows, columns: columns });
        });
    }
    bus.onGetReceived($scope, $scope.domain + ':one', function (data) {
        $scope.model = data.response;
        prepare();
    });

    init();
}]);