﻿App.controller('InsertItemFrameController', ['$scope', '$state', '$stateParams', 'HttpService', 'BusService', function ($scope, $state, $stateParams, hs, bus) {

    $scope.domain = $stateParams.domain;
    $scope.state = $state.current
    $scope.params = $stateParams;
    $scope.command = $stateParams.command;

    function init() {
        $('#insert-form-container').loading(function (lastback) {
            hs.getRequest($scope.domain + "/translations").then(function (lexicon) {
                $scope.lexicon = lexicon;
            });
        });
    }

    $scope.submitInsert = function () {
        hs.postRequest($scope.domain + "/insert", $scope.model).then(function (data) {
            bus.emitPostRequest($scope, $scope.domain + 'serviceorders:insert', data);
        });
    }

    bus.onGetReceived($scope, $scope.domain + ':translations', function (data) {
        $('#insert-form-container').unloading();
    });

    bus.onGetReceived($scope, 'insert-fields-got', function (data) {
        $('#insert-form-container').unloading();
    });

    bus.onGetReceived($scope, 'insertframe:fields', function (data) {
        $scope.fieldmodel = data.response;
        $scope.model = {};
        for (var i = 0; i < data.response.length; i++) {
            var field = data.response[i];
            Object.defineProperty($scope.model, field.name, {
                value: field.defaultValue,
                enumerable: false,
                writable: true,
            });
        }
        setTimeout(function () {
            $('.dtpicker-applied').datetimepicker({
                locale: 'tr'
            });
            if (/insert-.+/.test($scope.command)) {

            } else {
                $('.select2-plain').each(function (i, el) {
                    if ($(el).attr('data-url')) {
                        hs.getRequest($(el).attr('data-url')).then(function (list) {
                            $(el).select2({
                                data: list
                            });
                        });
                    } else {
                        $(el).select2();
                    }
                });
            }
            $('.select2-multiple').select2({ multiple: true });
        }, 20);

    });
    init();

}]);