﻿App.factory('MenuItemsFactory', function () {
    return function () {
        return [
            { title: "Giriş Sayfası", route: "index", iconClass: "fa-tachometer" },
            {
                title: "Arıza Bildirimleri", route: "#", iconClass: "fa-tachometer", submenu: [
                    { title: "İş Emirleri", route: "items-table({ domain: 'serviceorders' })", iconClass: "fa-list" },
                    { title: "Cariler", route: "items-table({ domain: 'currents' })", iconClass: "fa-user" },
                    //{ title: "İş Emri Düzenle", route: "edit-form({ domain: 'serviceorders', id: 1 })", iconClass: "fa-pencil-square-o" }
                ]
            },
        ];
    };
});