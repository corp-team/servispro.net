﻿using HS.App.Domain.Objects;
using System.Collections.Generic;

namespace HS.SPro.Seeds.Base {
    public interface  IModelSeeder<TModel>
        where TModel : IDomainBaseModel {

        IEnumerable<TModel> Collect();

        IModelSeeder<TModel> SeedData();

    }
}
