﻿using HS.App.Domain.Objects;
using CSC.App.Extensions.Static;
using System;
using System.Linq;

namespace HS.SPro.Seeds.Base {
    public static class SSeederFactory {

        public static IModelSeeder<TModel> Generate<TModel>()
                where TModel : IDomainBaseModel {
            var type = typeof(SSeederFactory).Assembly.GetTypes().Single(t => t.Implements(typeof(IModelSeeder<>).MakeGenericType(typeof(TModel))));
            return Activator.CreateInstance(type, new object[] { 45 }) as IModelSeeder<TModel>;
        }

    }
}
