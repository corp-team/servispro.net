﻿using Bogus;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Models.DomainObjects.Enums;
using HS.SPro.Seeds.Base;
using System;
using System.Collections.Generic;

namespace HS.SPro.Seeds.Seeders {
    internal class ServiceOrderSeeder : IModelSeeder<ServiceOrder> {

        private int N;
        public ServiceOrderSeeder(int n) {
            this.N = n;
        }

        public IEnumerable<ServiceOrder> Collect() {
            var id = 0;
            var testUsers = new Faker<ServiceOrder>()
                .RuleFor(u => u.ID, f => ++id)
                .RuleFor(u => u.CallCenterOperatorID, f => id)
                .RuleFor(u => u.DepartmentID, f => id)
                .RuleFor(u => u.CurrentID, f => id)
                .RuleFor(u => u.Description, f => f.Name.JobDescriptor())
                .RuleFor(u => u.DueDate, f => f.Date.Between(DateTime.Now, DateTime.Now.AddMonths(1)))
                .RuleFor(u => u.FulfilledOn, f => f.Date.Between(DateTime.Now, DateTime.Now.AddMonths(1)))
                .RuleFor(u => u.RequestedOn, f => DateTime.Now)
                .RuleFor(u => u.ServiceType, f => f.PickRandom<EServiceType>())
                .RuleFor(u => u.ServiceFulfillment, f => f.PickRandom<EServiceFulfillment>())
                .FinishWith((f, u) => {
                    Console.WriteLine("User Created! Id={0}", u.ID);
                });

            return testUsers.Generate(N);
        }

        public IModelSeeder<ServiceOrder> SeedData() {
            return this;
        }
    }
}
