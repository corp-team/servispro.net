﻿using Bogus;
using HS.SPro.Models.Aggregates;
using HS.SPro.Models.DomainObjects.Enums;
using HS.SPro.Seeds.Base;
using System;
using System.Collections.Generic;

namespace HS.SPro.Seeds.Seeders {
    internal class ServiceOrderAGSeeder : IModelSeeder<ServiceOrderAG> {

        private int N;
        public ServiceOrderAGSeeder(int n) {
            this.N = n;
        }

        public IEnumerable<ServiceOrderAG> Collect() {
            var id = 0;
            var testUsers = new Faker<ServiceOrderAG>()
                .RuleFor(u => u.ServiceOrderID, f => ++id)
                .RuleFor(u => u.ServiceOrderCurrentCode, f => "CSC" + f.Random.AlphaNumeric(5))
                .RuleFor(u => u.ServiceOrderCurrentName, f => f.Name.FirstName())
                .RuleFor(u => u.ServiceOrderDescription, f => f.Name.JobTitle())
                .RuleFor(u => u.ServiceOrderDueDate, f => f.Date.Between(DateTime.Now, DateTime.Now.AddMonths(1)))
                .RuleFor(u => u.ServiceOrderFulfilledOn, f => f.Date.Between(DateTime.Now, DateTime.Now.AddMonths(1)))
                .RuleFor(u => u.AssignedCallCenterOperatorName, f => f.Name.FirstName() + " " + f.Name.LastName())
                .RuleFor(u => u.ServiceOrderFulfillment, f => f.PickRandom<EServiceFulfillment>())
                .FinishWith((f, u) => {
                    Console.WriteLine("User Created! Id={0}", u.ServiceOrderID);
                });

            return testUsers.Generate(N);
        }

        public IModelSeeder<ServiceOrderAG> SeedData() {
            return this;
        }
    }
}
