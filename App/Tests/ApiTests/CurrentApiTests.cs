﻿
using HS.SPro.Models.Aggregates;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Tests.BaseClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace HS.SPro.Tests.ApiTests {
    [TestClass]
    public class CurrentApiTests : GenericAPITest<Current, CurrentAG, 
        SPro.Node1.Controllers.CurrentsController, RPI.Controllers.CurrentsController> { 
    }
}
