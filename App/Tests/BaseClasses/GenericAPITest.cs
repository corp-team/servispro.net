﻿using HS.App.Common.Decorators;
using HS.App.Domain.Aggregate;
using HS.App.Domain.Objects;
using CSC.App.Extensions.Static;

using HS.API.Common.Contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace HS.SPro.Tests.BaseClasses {
    public class GenericAPITest<TDomain, TAggregate, TDMController, TRPIController>
        where TDomain : DOBase<TDomain>
        where TAggregate : AGBase<TAggregate>
        where TRPIController : IRPIController<TAggregate>
        where TDMController : IApiController<TDomain> {

        [TestMethod]
        public async Task OneDomainWell() {
            var ctrl = Activator.CreateInstance<TDMController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var id = 1;
            var resp = (ctrl as IApiController<TDomain>).One(id);
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var content = await msg.Content.ReadAsStringAsync();
            var item = JsonConvert.DeserializeObject<TDomain>(content);
            Assert.IsNotNull(item);
            Assert.AreNotEqual(0, item.ID);
            Assert.AreEqual(id, item.ID);
        }

        [TestMethod]
        public async Task AllDomainWell() {
            var ctrl = Activator.CreateInstance<TDMController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var resp = (ctrl as IApiController<TDomain>).All();
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var items = JsonConvert.DeserializeObject<IEnumerable<TAggregate>>(await msg.Content.ReadAsStringAsync());
            Assert.IsNotNull(items);
            Assert.AreNotEqual(0, items.Count());
            Assert.IsTrue(items.All(it => it.IDProperty != 0));
        }

        [TestMethod]
        public async Task DomainFieldsWell() {
            var ctrl = Activator.CreateInstance<TDMController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var resp = (ctrl as IApiController<TDomain>).Fields(EFormType.Edit);
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var fields = JsonConvert.DeserializeObject<IEnumerable<IDictionary<string, object>>>(await msg.Content.ReadAsStringAsync());
            Assert.IsNotNull(fields);
            Assert.AreNotEqual(0, fields.Count());
        }

        [TestMethod]
        public async Task DomainListWell() {
            var ctrl = Activator.CreateInstance<TDMController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var resp = (ctrl as IApiController<TDomain>).List();
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var items = JsonConvert.DeserializeObject<IEnumerable<IDictionary<string, object>>>(await msg.Content.ReadAsStringAsync());
            Assert.IsNotNull(items);
            Assert.AreNotEqual(0, items.Count());
        }
        [TestMethod]
        public async Task DomainTranslationsWell() {
            var ctrl = Activator.CreateInstance<TDMController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var resp = (ctrl as IApiController<TDomain>).Translations();
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var items = JsonConvert.DeserializeObject<IDictionary<string, string>>(await msg.Content.ReadAsStringAsync());
            Assert.IsNotNull(items);
            Assert.AreNotEqual(0, items.Count());
            Assert.IsTrue(items.ContainsKey("InsertItemTitle"));
            Assert.IsTrue(items.ContainsKey("EditItemTitle"));
            Assert.IsTrue(items.ContainsKey("NewItemTitle"));
            Assert.IsTrue(items.ContainsKey("ItemsTableTitle"));
            Assert.IsTrue(items.ContainsKey("DisplayItemTitle"));
            Assert.IsTrue(items.ContainsKey("Entitlement"));
        }


        [TestMethod]
        public async Task OneRPIWell() {
            var ctrl = Activator.CreateInstance<TRPIController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var id = 1;
            var resp = await (ctrl as IRPIController<TAggregate>).One(id);
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var content = await msg.Content.ReadAsStringAsync();
            var item = JsonConvert.DeserializeObject<TDomain>(content);
            Assert.IsNotNull(item);
            Assert.AreNotEqual(0, item.ID);
            Assert.AreEqual(id, item.ID);
        }

        [TestMethod]
        public async Task AllRPIWell() {
            var ctrl = Activator.CreateInstance<TRPIController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var resp = await (ctrl as IRPIController<TAggregate>).All();
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var items = JsonConvert.DeserializeObject<IEnumerable<TAggregate>>(await msg.Content.ReadAsStringAsync());
            Assert.IsNotNull(items);
            Assert.AreNotEqual(0, items.Count());
            Assert.IsTrue(items.All(it => it.IDProperty != 0));
        }

        [TestMethod]
        public async Task RPIFieldsWell() {
            var ctrl = Activator.CreateInstance<TRPIController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var resp = await (ctrl as IRPIController<TAggregate>).Fields(EFormType.Edit);
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var fields = JsonConvert.DeserializeObject<IEnumerable<IDictionary<string, object>>>(await msg.Content.ReadAsStringAsync());
            Assert.IsNotNull(fields);
            Assert.AreNotEqual(0, fields.Count());
        }

        [TestMethod]
        public async Task RPIListWell() {
            var ctrl = Activator.CreateInstance<TRPIController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var resp = await (ctrl as IRPIController<TAggregate>).List();
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var items = JsonConvert.DeserializeObject<IEnumerable<IDictionary<string, object>>>(await msg.Content.ReadAsStringAsync());
            Assert.IsNotNull(items);
            Assert.AreNotEqual(0, items.Count());
        }

        [TestMethod]
        public async Task RPITranslationsWell() {
            var ctrl = Activator.CreateInstance<TRPIController>() as ApiController;
            ctrl.Request = new HttpRequestMessage();
            var resp = await (ctrl as IRPIController<TAggregate>).Translations();
            var token = new CancellationToken();
            var msg = await resp.ExecuteAsync(token);
            Assert.IsTrue(msg.IsSuccessStatusCode);
            var items = JsonConvert.DeserializeObject<IDictionary<string, string>>(await msg.Content.ReadAsStringAsync());
            Assert.IsNotNull(items);
            Assert.AreNotEqual(0, items.Count());
            Assert.IsTrue(items.ContainsKey("InsertItemTitle"));
            Assert.IsTrue(items.ContainsKey("EditItemTitle"));
            Assert.IsTrue(items.ContainsKey("NewItemTitle"));
            Assert.IsTrue(items.ContainsKey("ItemsTableTitle"));
            Assert.IsTrue(items.ContainsKey("DisplayItemTitle"));
        }

    }
}
