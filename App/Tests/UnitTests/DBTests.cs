﻿using HS.App.Domain.Data;
using HS.App.Domain.DML;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Models.DomainObjects.Enums;
using HS.SPro.Models.DomainObjects.Parameters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace HS.SPro.Tests.UnitTests {

    [TestClass]
    public class DBTests {

        [TestMethod]
        public void DbSeedsWell() {

            SBuildEngine.BuildDomain(typeof(ServiceOrder).Assembly.GetName());

            Action<Exception> failback = (ex) => Assert.Fail(ex.Message);
            RebuildOperators(failback);
            RebuildCallCenterOperators(failback);
            RebuildDepartments(failback);
            RebuildCountries(failback);
            RebuildStates(failback);
            RebuildDistricts(failback);
            RebuildAddresses(failback);
            RebuildCurrents(failback);
            RebuildCallers(failback);
            RebuildServiceOrders(failback);
        }

        private static void RebuildOperators(Action<Exception> failback) {
            var operators = new List<Tuple<int, string, string>>() {
                Tuple.Create(1, "CENGİZ KARABULUT", "05326710450"),
                Tuple.Create(2, "YAKUP KARABULUT", "05415231380"),
                Tuple.Create(3, "EMRE YÜKSEKDAĞ", "05324538353"),
                Tuple.Create(4, "SEÇKİN CANDAN", "05324770996"),
                Tuple.Create(5, "MEDİNE GEL", "05074952325"),
                Tuple.Create(6, "ERMAN UĞUR", "05078657322"),
                Tuple.Create(7, "YUNUS EMRE ", "05418820146"),
                Tuple.Create(8, "HÜSEYİN SÖNMEZ", "05360331719"),
                Tuple.Create(9, "MESUT ÇALIK", "05459074591")
            };
            foreach (var op in operators) {
                CR<ServiceOperator>.CRUD(new ServiceOperator() {
                    ID = op.Item1,
                    FullName = op.Item2,
                    MobilePhone = op.Item3,
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(op.Item1, xq.ID));
            }
        }
        private static void RebuildCallCenterOperators(Action<Exception> failback) {

            var operators = new List<Tuple<int, string, string, string, string>>() {
                Tuple.Create(1, "MEDİNE GEL", "05074952325", "medine@cscgrup.com", "1000")
            };
            foreach (var op in operators) {
                CR<CallCenterOperator>.CRUD(new CallCenterOperator() {
                    ID = op.Item1,
                    FullName = op.Item2,
                    MobilePhone = op.Item3,
                    Email = op.Item4,
                    Line = op.Item5,
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(op.Item1, xq.ID));
            }
        }
        private static void RebuildDepartments(Action<Exception> failback) {
            var departments = new List<Tuple<int, string, string>>() {
                Tuple.Create(1, "İdare", "CENGİZ KARABULUT"),
                Tuple.Create(2, "Servis", "YAKUP KARABULUT")
            };
            foreach (var dp in departments) {
                CR<Department>.CRUD(new Department() {
                    ID = dp.Item1,
                    Description = dp.Item2,
                    ResponsibleOfficer = dp.Item3,
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(dp.Item1, xq.ID));
            }
        }
        private static void RebuildCountries(Action<Exception> failback) {
            var countries = new List<Tuple<int, string>>() {
                Tuple.Create<int, string>(1, "Türkiye"),
            };
            foreach (var it in countries) {
                CR<Country>.CRUD(new Country() {
                    ID = it.Item1,
                    Description = it.Item2
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(it.Item1, xq.ID));
            }
        }
        private static void RebuildStates(Action<Exception> failback) {
            var states = new List<Tuple<int, string, int>>() {
                Tuple.Create(1, "Düzce", 81),
                Tuple.Create(2, "Bolu", 14)
            };
            foreach (var it in states) {
                CR<State>.CRUD(new State() {
                    ID = it.Item1,
                    Description = it.Item2,
                    PlateCode = it.Item3,
                    CountryID = 1
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(it.Item1, xq.ID));
            }
        }
        private static void RebuildDistricts(Action<Exception> failback) {
            var districts = new List<Tuple<int, string>>() {
                Tuple.Create(1, "Düzce Merkez")
            };
            foreach (var it in districts) {
                CR<District>.CRUD(new District() {
                    ID = it.Item1,
                    Description = it.Item2,
                    StateID = 1
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(it.Item1, xq.ID));
            }
        }
        private static void RebuildAddresses(Action<Exception> failback) {
            var addresses = new List<Tuple<int, string>>() {
                Tuple.Create(1, "Merkez Ofis"),
                Tuple.Create(2, "Bayi")
            };
            foreach (var it in addresses) {
                CR<Address>.CRUD(new Address() {
                    ID = it.Item1,
                    Description = it.Item2,
                    CountryID = 1,
                    StateID = 1,
                    DistrictID = 1
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(it.Item1, xq.ID));
            }
        }
        private static void RebuildCallers(Action<Exception> failback) {
            var customers = new List<Tuple<int, string, string>>() {
                Tuple.Create(1, "KENAN ÖZKURT", "5645645641654"),
                Tuple.Create(2, "KEMAL DENİZ", "05320331719")
            };
            foreach (var it in customers) {
                CR<Caller>.CRUD(new Caller() {
                    ID = it.Item1,
                    FullName = it.Item2,
                    MobilePhone = it.Item3,
                    CurrentID = 1
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(it.Item1, xq.ID));
            }
        }
        private static void RebuildCurrents(Action<Exception> failback) {
            var currents = new List<Tuple<int, string, string>>() {
                Tuple.Create(1, "CSC GRUP Bilgisayar", "03805231380")
            };
            var c = 0;
            foreach (var it in currents) {
                CR<Current>.CRUD(new Current() {
                    ID = it.Item1,
                    FirmName = it.Item2,
                    CurrentCode = "CSC" + (++c).ToString().PadLeft(5, '0'),
                    Phone = it.Item3,
                    AddressID = 1,
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(it.Item1, xq.ID));
            }
        }
        private static void RebuildServiceOrders(Action<Exception> failback) {
            var orders = new List<Tuple<int, string, string, DateTime>>() {
                Tuple.Create(1, "Ali Kaya", "Kamera Montajı", DateTime.Now.AddDays(3)),
            };
            foreach (var it in orders) {
                CR<ServiceOrder>.CRUD(new ServiceOrder() {
                    ID = it.Item1,
                    Description = it.Item3,
                    ServiceFulfillment = EServiceFulfillment.Working,
                    RequestedOn = DateTime.Now,
                    DueDate = it.Item4,
                    ServiceType = EServiceType.HandOn,
                    CallCenterOperatorID = 1,
                    CurrentID = 1,
                    DepartmentID = 1,
                    CallerID = 1,
                }).SetAutoIdentity(false).Upsert(xq => Assert.AreEqual(it.Item1, xq.ID));
            }
        }

    }
}
