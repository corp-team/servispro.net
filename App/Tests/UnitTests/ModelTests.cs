﻿using HS.SPro.Models.Aggregates;
using HS.SPro.Models.DomainObjects.Core;
using HS.SPro.Seeds.Base;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace HS.SPro.Tests.UnitTests {
    [TestClass]
    public class ModelTests {
        [TestMethod]
        public void SeedCollectWell() {
            var seed = SSeederFactory.Generate<ServiceOrder>();
            Assert.AreNotEqual(0, seed.Collect().Count());
            Assert.AreEqual(45, seed.Collect().Count());
        }

        [TestMethod]
        public void SeedCollectAGWell() {
            var seed = SSeederFactory.Generate<ServiceOrderAG>();
            Assert.AreNotEqual(0, seed.Collect().Count());
            Assert.AreEqual(45, seed.Collect().Count());
        }

    }
}
