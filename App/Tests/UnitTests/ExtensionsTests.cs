﻿using CSC.App.Extensions.Static;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace HS.SPro.Tests.ApiTests {

    [TestClass]
    public class ExtensionsTests {

        [TestMethod]
        public void IsCollectionWell() {

            Assert.IsFalse(typeof(string).IsCollection());
            Assert.IsFalse(typeof(Guid).IsCollection());
            Assert.IsTrue(typeof(List<string>).IsCollection());

        }

    }

}
